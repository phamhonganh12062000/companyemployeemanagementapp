﻿using Entities.Exceptions;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Behaviors
{
    public sealed class ValidationBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {

        private readonly IEnumerable<IValidator<TRequest>> _requestValidators;

        public ValidationBehavior(IEnumerable<IValidator<TRequest>> requestValidators)
        {
            _requestValidators = requestValidators;
        }

        public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
        {
            if (!HasValidationErrors())
            {
                return await next();
            }

            ThrowValidationAppException(request);

            return await next();
        }

        private bool HasValidationErrors()
        {
            return _requestValidators.Any();
        }

        private bool HasErrorsInDictionary(Dictionary<string, string[]> errorsDictionary) 
        {
            return errorsDictionary.Any();
        }

        private void ThrowValidationAppException(TRequest request)
        {
            var errorsDictionary = ExtractErrorsFromRequestValidators(request);

            if (HasErrorsInDictionary(errorsDictionary))
            {
                throw new ValidationAppException(errorsDictionary);
            }
        }

        private Dictionary<string, string[]> ExtractErrorsFromRequestValidators(TRequest request)
        {
            var context = new ValidationContext<TRequest>(request);

            var errorsDictionary = _requestValidators
                .Select(x => x.Validate(context))
                .SelectMany(x => x.Errors)
                .Where(x => x != null)
                .GroupBy(
                    x => x.PropertyName.Substring(x.PropertyName.IndexOf('.') + 1),
                    x => x.ErrorMessage,
                    // Add a tuple to a dictionary
                    (propertyName, errorMessages) => new
                    {
                        Key = propertyName,
                        Values = errorMessages.Distinct().ToArray()
                    })
                .ToDictionary(x => x.Key, x => x.Values);

            return errorsDictionary;
        }
    }
}
