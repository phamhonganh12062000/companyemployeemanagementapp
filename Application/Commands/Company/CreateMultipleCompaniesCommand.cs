﻿using MediatR;
using Shared.Dtos.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commands.Company
{
    public sealed record CreateMultipleCompaniesCommand(IEnumerable<CompanyForCreationDto> Companies) : IRequest<(IEnumerable<CompanyDto>, string)>
    {
    }
}
