﻿using MediatR;
using Shared.Dtos.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commands.Company
{
    // Blank IRequest generic parameter => No returning any value
    public sealed record UpdateSingleCompanyCommand(Guid Id, CompanyForUpdateDto CompanyForUpdateDto, bool TrackChanges) : IRequest
    {
    }
}
