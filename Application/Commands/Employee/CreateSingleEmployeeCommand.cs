﻿using Entities.Responses.Success;
using MediatR;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commands.Employee
{
    public sealed record CreateSingleEmployeeCommand(Guid CompanyId, EmployeeForCreationDto Employee, bool TrackChanges) : IRequest<ApiOkResponse<EmployeeDto>>
    {
    }


}
