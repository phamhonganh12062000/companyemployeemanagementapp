﻿using MediatR;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commands.Employee
{
    public sealed record UpdateSingleEmployeeCommand(Guid CompanyId, Guid Id, EmployeeForUpdateDto Employee, bool CompanyTrackChanges, bool EmployeeTrackChanges) : IRequest
    {
    }
}
