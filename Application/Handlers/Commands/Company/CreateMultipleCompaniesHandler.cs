﻿using Application.Commands.Company;
using AutoMapper;
using Contracts;
using Entities.Exceptions.BadRequest;
using Entities.Models;
using MediatR;
using Shared.Dtos.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Commands.Company
{
    internal sealed class CreateMultipleCompaniesHandler : IRequestHandler<CreateMultipleCompaniesCommand, (IEnumerable<CompanyDto>, string)>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateMultipleCompaniesHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper; 
            _unitOfWork = unitOfWork;
        }

        public async Task<(IEnumerable<CompanyDto>, string)> Handle(CreateMultipleCompaniesCommand request, CancellationToken cancellationToken)
        {
            if (request.Companies is null)
            {
                throw new CompanyCollectionBadRequest();
            }

            var companyEntities = _mapper.Map<IEnumerable<Entities.Models.Company>>(request.Companies);

            foreach (var company in companyEntities)
            {
                _unitOfWork.CompanyRepository.CreateCompany(company);
            }

            await _unitOfWork.SaveAsync();

            var companyCollectionToReturn = _mapper.Map<IEnumerable<CompanyDto>>(companyEntities);

            var ids = string.Join(",", companyCollectionToReturn.Select(c => c.Id));

            return (companyCollectionToReturn, ids);
        }
    }
}
