﻿using Application.Commands;
using Application.Commands.Company;
using AutoMapper;
using Contracts;
using Entities.Models;
using MediatR;
using Shared.Dtos.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Commands.Company
{
    internal sealed class CreateSingleCompanyHandler : IRequestHandler<CreateSingleCompanyCommand, CompanyDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateSingleCompanyHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<CompanyDto> Handle(CreateSingleCompanyCommand request, CancellationToken cancellationToken)
        {
            var companyEntity = _mapper.Map<Entities.Models.Company>(request.Company);

            _unitOfWork.CompanyRepository.CreateCompany(companyEntity);

            await _unitOfWork.SaveAsync();

            return _mapper.Map<CompanyDto>(companyEntity);
        }
    }
}
