﻿using Application.Notifications;
using Contracts;
using Entities.Exceptions.NotFound;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Commands.Company
{
    internal sealed class DeleteSingleCompanyHandler : INotificationHandler<CompanyDeletedNotification>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteSingleCompanyHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task Handle(CompanyDeletedNotification notification, CancellationToken cancellationToken)
        {
            var companyFromDb = await _unitOfWork.CompanyRepository.GetCompanyAsync(notification.Id, notification.TrackChanges);

            if (companyFromDb is null)
            {
                throw new CompanyNotFoundException(notification.Id);
            }

            _unitOfWork.CompanyRepository.DeleteCompany(companyFromDb);

            await _unitOfWork.SaveAsync();
        }
    }
}
