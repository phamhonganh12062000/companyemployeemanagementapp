﻿using Application.Commands.Company;
using AutoMapper;
using Contracts;
using Entities.Exceptions.NotFound;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Commands.Company
{
    internal sealed class UpdateSingleCompanyHandler : IRequestHandler<UpdateSingleCompanyCommand, Unit>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateSingleCompanyHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdateSingleCompanyCommand request, CancellationToken cancellationToken)
        {
            var companyFromDb = await _unitOfWork.CompanyRepository.GetCompanyAsync(request.Id, request.TrackChanges);

            if (companyFromDb is null)
            {
                throw new CompanyNotFoundException(request.Id);
            }

            _mapper.Map(request.CompanyForUpdateDto, companyFromDb);

            await _unitOfWork.SaveAsync();

            return Unit.Value; // Return the void type
        }
    }
}
