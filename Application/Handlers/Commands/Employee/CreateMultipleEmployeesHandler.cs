﻿using Application.Commands.Employee;
using AutoMapper;
using Contracts;
using Entities.Exceptions.BadRequest;
using Entities.Exceptions.NotFound;
using MediatR;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Commands.Employee
{
    internal sealed class CreateMultipleEmployeesHandler : IRequestHandler<CreateMultipleEmployeesCommand, (IEnumerable<EmployeeDto>, string)>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateMultipleEmployeesHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<(IEnumerable<EmployeeDto>, string)> Handle(CreateMultipleEmployeesCommand request, CancellationToken cancellationToken)
        {
            if (request.Employees is null)
            {
                throw new EmployeeCollectionBadRequest();
            }

            var company = await _unitOfWork.CompanyRepository.GetCompanyAsync(request.CompanyId, request.TrackChanges);

            if (company is null)
            {
                throw new CompanyNotFoundException(request.CompanyId);
            }

            var employeesToCreate = _mapper.Map<IEnumerable<Entities.Models.Employee>>(request.Employees);

            foreach (var employee in employeesToCreate)
            {
                _unitOfWork.EmployeeRepository.CreateEmployeeForACompany(request.CompanyId, employee);
            }

            await _unitOfWork.SaveAsync();

            var employeeCollectionToReturn = _mapper.Map<IEnumerable<EmployeeDto>>(employeesToCreate);

            var ids = string.Join(",", employeeCollectionToReturn.Select(e => e.Id));

            return (employees: employeeCollectionToReturn, ids);
        }
    }
}
