﻿using Application.Commands.Employee;
using AutoMapper;
using Contracts;
using Entities.Exceptions.NotFound;
using Entities.Responses.Success;
using MediatR;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Commands.Employee
{
    internal sealed class CreateSingleEmployeeHandler : IRequestHandler<CreateSingleEmployeeCommand, ApiOkResponse<EmployeeDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateSingleEmployeeHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ApiOkResponse<EmployeeDto>> Handle(CreateSingleEmployeeCommand request, CancellationToken cancellationToken)
        {
            var company = await _unitOfWork.CompanyRepository.GetCompanyAsync(request.CompanyId, request.TrackChanges);

            if (company is null)
            {
                throw new CompanyNotFoundException(request.CompanyId);
            }

            var employeeToBeCreated = _mapper.Map<Entities.Models.Employee>(request.Employee);

            _unitOfWork.EmployeeRepository.CreateEmployeeForACompany(request.CompanyId, employeeToBeCreated);

            await _unitOfWork.SaveAsync();

            var employeeToReturn = _mapper.Map<EmployeeDto>(employeeToBeCreated);

            return new ApiOkResponse<EmployeeDto>(employeeToReturn);
        }
    }
}
