﻿using Application.Notifications;
using Contracts;
using Entities.Exceptions.NotFound;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Commands.Employee
{
    internal sealed class DeleteSingleEmployeeHandler : INotificationHandler<EmployeeDeletedNotification>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteSingleEmployeeHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task Handle(EmployeeDeletedNotification notification, CancellationToken cancellationToken)
        {
            var company = await _unitOfWork.CompanyRepository.GetCompanyAsync(notification.CompanyId, notification.TrackChanges);

            if (company is null)
            {
                throw new CompanyNotFoundException(notification.CompanyId);
            }

            var employee = await _unitOfWork.EmployeeRepository.GetEmployeeOfACompanyAsync(notification.CompanyId, notification.Id, notification.TrackChanges);

            if (employee is null)
            {
                throw new EmployeeNotFoundException(notification.Id);
            }

            _unitOfWork.EmployeeRepository.DeleteEmployee(employee);

            await _unitOfWork.SaveAsync();
        }
    }
}
