﻿using Application.Commands.Employee;
using AutoMapper;
using Contracts;
using Entities.Exceptions.NotFound;
using MediatR;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Commands.Employee
{
    internal sealed class UpdateSingleEmployeeHandler : IRequestHandler<UpdateSingleEmployeeCommand, Unit>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateSingleEmployeeHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdateSingleEmployeeCommand request, CancellationToken cancellationToken)
        {
            var company = await _unitOfWork.CompanyRepository.GetCompanyAsync(request.CompanyId, request.CompanyTrackChanges);

            if (company is null)
            {
                throw new CompanyNotFoundException(request.CompanyId);
            }

            var employee = await _unitOfWork.EmployeeRepository.GetEmployeeOfACompanyAsync(request.CompanyId, request.Id, request.EmployeeTrackChanges);

            if (employee is null)
            {
                throw new EmployeeNotFoundException(request.Id);
            }

            // Connected update: Use the same context object to fetch the entity and to update it
            _mapper.Map(request.Employee, employee);

            await _unitOfWork.SaveAsync();

            return Unit.Value;
        }
    }
}
