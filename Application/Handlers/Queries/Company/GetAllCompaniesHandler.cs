﻿using Application.Queries.Company;
using AutoMapper;
using Contracts;
using Contracts.EntityLinkContracts;
using Entities.LinksModel;
using Entities.Responses;
using Entities.Responses.Success;
using MediatR;
using Shared.Dtos.Company;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Queries.Company
{
    /// <summary>
    /// Replacement for the service layer method.
    /// </summary>
    internal sealed class GetAllCompaniesHandler : IRequestHandler<GetAllCompaniesQuery, (ApiBaseResponse response, MetaData metaData)>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICompanyLinks _companyLinks;

        public GetAllCompaniesHandler(IUnitOfWork unitOfWork, IMapper mapper, ICompanyLinks companyLinks)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _companyLinks = companyLinks;

        }

        public async Task<(ApiBaseResponse response, MetaData metaData)> Handle(GetAllCompaniesQuery request, CancellationToken cancellationToken)
        {
            var companiesWithMetaData = await _unitOfWork.CompanyRepository.GetAllCompaniesAsync(request.LinkParamaters.CompanyParameters, request.TrackChanges);

            var companiesDto = _mapper.Map<IEnumerable<CompanyDto>>(companiesWithMetaData);

            var companyWithLinks = _companyLinks.TryGenerateLinks(companiesDto, request.LinkParamaters.CompanyParameters.Fields, request.LinkParamaters.HttpContext);

            return (response: new ApiOkResponse<LinkResponse>(companyWithLinks), metaData: companiesWithMetaData.MetaData);
        }
    }
}
