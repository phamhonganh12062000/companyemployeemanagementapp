﻿using Application.Queries.Company;
using AutoMapper;
using Contracts;
using Contracts.EntityLinkContracts;
using Entities.Exceptions.BadRequest;
using Entities.LinksModel;
using Entities.Responses;
using Entities.Responses.Success;
using MediatR;
using Shared.Dtos.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Queries.Company
{
    internal sealed class GetMultipleCompaniesHandler : IRequestHandler<GetMultipleCompaniesQuery, ApiBaseResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICompanyLinks _companyLinks;

        public GetMultipleCompaniesHandler(IUnitOfWork unitOfWork, IMapper mapper, ICompanyLinks companyLinks)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _companyLinks = companyLinks;

        }

        public async Task<ApiBaseResponse> Handle(GetMultipleCompaniesQuery request, CancellationToken cancellationToken)
        {
            if (request.Ids is null)
            {
                throw new IdParametersBadRequestException();
            }

            var collectionOfCompanies = await _unitOfWork.CompanyRepository.GetMultipleCompaniesByIdsAsync(
                request.Ids,
                request.LinkParameters.CompanyParameters,
                request.TrackChanges);

            if (request.Ids.Count() != collectionOfCompanies.Count())
            {
                throw new CollectionByIdsBadRequestException();
            }

            var companyCollectionDtos = _mapper.Map<IEnumerable<CompanyDto>>(collectionOfCompanies);

            var companyWithLinks = _companyLinks.TryGenerateLinks(
                companyCollectionDtos, request.LinkParameters.CompanyParameters.Fields, request.LinkParameters.HttpContext);

            return new ApiOkResponse<LinkResponse>(companyWithLinks);
        }
    }
}
