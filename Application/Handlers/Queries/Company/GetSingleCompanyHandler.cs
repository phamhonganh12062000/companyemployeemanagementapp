﻿using Application.Queries.Company;
using AutoMapper;
using Contracts;
using Entities.Exceptions.NotFound;
using Entities.Models;
using Entities.Responses;
using Entities.Responses.Success;
using MediatR;
using Shared.Dtos.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Queries.Company
{
    /// <summary>
    /// Replacement for the service layer method 
    /// </summary>
    internal sealed class GetSingleCompanyHandler : IRequestHandler<GetSingleCompanyQuery, ApiBaseResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetSingleCompanyHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ApiBaseResponse> Handle(GetSingleCompanyQuery request, CancellationToken cancellationToken)
        {
            var companyDto = _mapper.Map<CompanyDto>(await FetchCompanyAndCheckIfNullAsync(request));

            return new ApiOkResponse<CompanyDto>(companyDto);
        }

        private async Task<Entities.Models.Company> FetchCompanyAndCheckIfNullAsync(GetSingleCompanyQuery request)
        {
            var company = await _unitOfWork.CompanyRepository.GetCompanyAsync(request.Id, request.TrackChanges);

            if (company is null)
            {
                throw new CompanyNotFoundException(request.Id);
            }

            return company;
        }
    }
}
