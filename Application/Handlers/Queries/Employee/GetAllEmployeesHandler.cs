﻿using Application.Queries.Employee;
using AutoMapper;
using Contracts;
using Contracts.EntityLinkContracts;
using Entities.Exceptions.BadRequest;
using Entities.Exceptions.NotFound;
using Entities.LinksModel;
using Entities.Responses;
using Entities.Responses.Success;
using MediatR;
using Shared.Dtos.Employee;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Queries.Employee
{
    internal sealed class GetAllEmployeesHandler : IRequestHandler<GetAllEmployeesQuery, (ApiBaseResponse response, MetaData metaData)>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IEmployeeLinks _employeeLinks;

        public GetAllEmployeesHandler(IUnitOfWork unitOfWork, IMapper mapper, IEmployeeLinks employeeLinks)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _employeeLinks = employeeLinks;

        }

        public async Task<(ApiBaseResponse response, MetaData metaData)> Handle(GetAllEmployeesQuery request, CancellationToken cancellationToken)
        {
            if (!request.LinkParameters.EmployeeParameters.ValidAgeRange)
            {
                throw new MaxAgeRangeBadRequestException();
            }

            var company = await _unitOfWork.CompanyRepository.GetCompanyAsync(request.CompanyId, request.TrackChanges);

            if (company is null)
            {
                throw new CompanyNotFoundException(request.CompanyId);
            }

            var employeesWithMetaData = await _unitOfWork.EmployeeRepository
                .GetAllEmployeesOfACompanyAsync(request.CompanyId, request.LinkParameters.EmployeeParameters, request.TrackChanges);

            var employeeDtos = _mapper.Map<IEnumerable<EmployeeDto>>(employeesWithMetaData);

            var employeesWithLinks = _employeeLinks.TryGenerateLinks(employeeDtos,
                request.LinkParameters.EmployeeParameters.Fields,
                request.CompanyId, request.LinkParameters.HttpContext);

            return (response: new ApiOkResponse<LinkResponse>(employeesWithLinks), metaData: employeesWithMetaData.MetaData);
        }
    }
}
