﻿using Application.Queries.Employee;
using AutoMapper;
using Contracts;
using Contracts.EntityLinkContracts;
using Entities.Exceptions.BadRequest;
using Entities.Exceptions.NotFound;
using Entities.LinksModel;
using Entities.Responses;
using Entities.Responses.Success;
using MediatR;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Queries.Employee
{
    internal sealed class GetMultipleEmployeesHandler : IRequestHandler<GetMultipleEmployeesQuery, ApiBaseResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IEmployeeLinks _employeeLinks;

        public GetMultipleEmployeesHandler(IMapper mapper, IUnitOfWork unitOfWork, IEmployeeLinks employeeLinks)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _employeeLinks = employeeLinks;

        }

        public async Task<ApiBaseResponse> Handle(GetMultipleEmployeesQuery request, CancellationToken cancellationToken)
        {
            if (request.Ids is null)
            {
                throw new IdParametersBadRequestException();
            }

            var company = await _unitOfWork.CompanyRepository.GetCompanyAsync(request.CompanyId, request.TrackChanges);

            if (company is null)
            {
                throw new CompanyNotFoundException(request.CompanyId);
            }

            var collectionOfEmployees = await _unitOfWork.EmployeeRepository.GetMultipleEmployeesOfACompanyByIdsAsync(
                request.CompanyId, request.Ids, request.LinkParameters.EmployeeParameters, request.TrackChanges);

            if (request.Ids.Count() != collectionOfEmployees.Count())
            {
                throw new CollectionByIdsBadRequestException();
            }

            var employeeDtos = _mapper.Map<IEnumerable<EmployeeDto>>(collectionOfEmployees);

            var employeesWithLinks = _employeeLinks.TryGenerateLinks(employeeDtos,
                request.LinkParameters.EmployeeParameters.Fields,
                request.CompanyId, request.LinkParameters.HttpContext);

            return new ApiOkResponse<LinkResponse>(employeesWithLinks);
        }
    }
}
