﻿using Application.Queries.Employee;
using AutoMapper;
using Contracts;
using Entities.Exceptions.NotFound;
using Entities.Responses;
using Entities.Responses.Success;
using MediatR;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Queries.Employee
{
    internal sealed class GetSingleEmployeeHandler : IRequestHandler<GetSingleEmployeeQuery, ApiBaseResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetSingleEmployeeHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ApiBaseResponse> Handle(GetSingleEmployeeQuery request, CancellationToken cancellationToken)
        {
            var company = await _unitOfWork.CompanyRepository.GetCompanyAsync(request.CompanyId, request.TrackChanges);

            if (company is null)
            {
                throw new CompanyNotFoundException(request.CompanyId);
            }

            var employee = await _unitOfWork.EmployeeRepository.GetEmployeeOfACompanyAsync(request.CompanyId, request.Id, request.TrackChanges);

            if (employee is null)
            {
                throw new EmployeeNotFoundException(request.Id);
            }

            var employeeDto = _mapper.Map<EmployeeDto>(employee);

            return new ApiOkResponse<EmployeeDto>(employeeDto);
        }
    }
}
