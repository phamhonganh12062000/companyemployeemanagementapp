﻿using Application.Notifications;
using Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Utilities
{
    internal sealed class CompanyEmailHandler : INotificationHandler<CompanyDeletedNotification>
    {
        private readonly ILoggerManager _logger;

        public CompanyEmailHandler(ILoggerManager logger)
        {
            _logger = logger;
        }

        public async Task Handle(CompanyDeletedNotification notification, CancellationToken cancellationToken)
        {
            // TODO: Fix this does not log in the console
            _logger.LogWarning($"Delete action for the company with id: {notification.Id} has been executed.");

            await Task.CompletedTask;
        }
    }
}
