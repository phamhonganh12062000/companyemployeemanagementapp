﻿using Application.Notifications;
using Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Handlers.Utilities
{
    internal sealed class EmployeeEmailHandler : INotificationHandler<EmployeeDeletedNotification>
    {
        private readonly ILoggerManager _logger;

        public EmployeeEmailHandler(ILoggerManager logger)
        {
            _logger = logger;
        }

        public async Task Handle(EmployeeDeletedNotification notification, CancellationToken cancellationToken)
        {
            _logger.LogWarning($"Delete action for the employee with id: {notification.Id} has been executed.");

            await Task.CompletedTask;
        }
    }
}
