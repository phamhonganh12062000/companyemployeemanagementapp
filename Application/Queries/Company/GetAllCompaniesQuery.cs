﻿using Entities.LinksModel;
using Entities.Responses;
using MediatR;
using Shared.Dtos.Company;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Queries.Company
{
    public sealed record GetAllCompaniesQuery(LinkParameters LinkParamaters, bool TrackChanges) : IRequest<(ApiBaseResponse response, MetaData metaData)>
    {
    }
}
