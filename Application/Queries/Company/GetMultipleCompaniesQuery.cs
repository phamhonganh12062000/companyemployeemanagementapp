﻿using Entities.LinksModel;
using Entities.Responses;
using MediatR;
using Shared.Dtos.Company;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Queries.Company
{
    public sealed record GetMultipleCompaniesQuery(IEnumerable<Guid> Ids, LinkParameters LinkParameters, bool TrackChanges) : IRequest<ApiBaseResponse>
    {
    }
}
