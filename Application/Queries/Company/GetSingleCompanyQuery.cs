﻿using Entities.Responses;
using MediatR;
using Shared.Dtos.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Queries.Company
{
    public sealed record GetSingleCompanyQuery(Guid Id, bool TrackChanges) : IRequest<ApiBaseResponse>
    {
    }
}
