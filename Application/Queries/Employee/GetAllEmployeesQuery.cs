﻿using Entities.LinksModel;
using Entities.Responses;
using MediatR;
using Shared.Dtos.Employee;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Queries.Employee
{
    public sealed record GetAllEmployeesQuery(Guid CompanyId, LinkParameters LinkParameters, bool TrackChanges) : IRequest<(ApiBaseResponse response, MetaData metaData)>
    {
    }
}
