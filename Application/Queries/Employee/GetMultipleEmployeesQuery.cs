﻿using Entities.LinksModel;
using Entities.Responses;
using Entities.Responses.Success;
using MediatR;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Queries.Employee
{
    public sealed record GetMultipleEmployeesQuery(Guid CompanyId, IEnumerable<Guid> Ids, LinkParameters LinkParameters, bool TrackChanges) 
        : IRequest<ApiBaseResponse>
    {
    }
}
