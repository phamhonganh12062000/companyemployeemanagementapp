﻿using Application.Commands.Company;
using FluentValidation;
using FluentValidation.Results;
using Shared.Dtos.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Validators
{
    public sealed class CreateSingleCompanyCommandValidator : AbstractValidator<CreateSingleCompanyCommand>
    {
        public CreateSingleCompanyCommandValidator()
        {
            RuleFor(c => c.Company.Name).NotEmpty().MaximumLength(60);
            RuleFor(c => c.Company.Address).NotEmpty().MaximumLength(60);
        }

        public override ValidationResult Validate(ValidationContext<CreateSingleCompanyCommand> context)
        {
            return context.InstanceToValidate.Company is null
                ? new ValidationResult(new []
                {
                    // Adding new ValidationFailure here is allowed
                    new ValidationFailure(nameof(CompanyForCreationDto), nameof(CompanyForCreationDto) + " object is null."),
                }) : base.Validate(context);
        }
    }
}
