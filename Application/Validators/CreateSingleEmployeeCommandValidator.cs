﻿using Application.Commands.Employee;
using FluentValidation;
using FluentValidation.Results;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Validators
{
    public sealed class CreateSingleEmployeeCommandValidator : AbstractValidator<CreateSingleEmployeeCommand>
    {
        public CreateSingleEmployeeCommandValidator()
        {
            RuleFor(e => e.Employee.Name).NotEmpty().MaximumLength(30);
            RuleFor(e => e.Employee.Age).NotEmpty().InclusiveBetween(18, int.MaxValue);
            RuleFor(e => e.Employee.Position).NotEmpty().MaximumLength(20);
        }

        public override ValidationResult Validate(ValidationContext<CreateSingleEmployeeCommand> context)
        {
            return context.InstanceToValidate.Employee is null
                ? new ValidationResult(new[]
                {
                    new ValidationFailure(nameof(EmployeeForCreationDto), nameof(EmployeeForCreationDto) + " object is null."),
                }) : base.Validate(context);
        }
    }
}
