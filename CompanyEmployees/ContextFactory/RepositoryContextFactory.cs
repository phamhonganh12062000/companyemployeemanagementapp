﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Repository;

namespace CompanyEmployees.ContextFactory
{
    public class RepositoryContextFactory : IDesignTimeDbContextFactory<RepositoryContext>
    {
        /// <summary>
        /// Register RepositoryContext class at design time
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public RepositoryContext CreateDbContext(string[] args)
        {
            var configurations = new ConfigurationBuilder()
                .AddUserSecrets<Program>()
                .Build();

            var builder = new DbContextOptionsBuilder<RepositoryContext>()
                .UseSqlServer(configurations.GetConnectionString("sqlConnection"), 
                b => b.MigrationsAssembly("CompanyEmployees"));

            return new RepositoryContext(builder.Options);

        }
    }
}
