﻿using Contracts;
using Entities.ErrorModels;
using Entities.Exceptions;
using Entities.Exceptions.BadRequest;
using Entities.Exceptions.NotFound;
using Microsoft.AspNetCore.Diagnostics;
using System.Text.Json;

namespace CompanyEmployees.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this WebApplication app, ILoggerManager logger)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    // Add a terminal middleware delegate to the application pipeline
                    context.Response.ContentType = "application/json";

                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();

                    if (contextFeature != null)
                    {

                        context.Response.StatusCode = contextFeature.Error switch
                        {
                            NotFoundException => StatusCodes.Status404NotFound,
                            BadRequestException => StatusCodes.Status400BadRequest,
                            ValidationAppException => StatusCodes.Status422UnprocessableEntity,
                            _ => StatusCodes.Status500InternalServerError
                        };

                        // Apply Declaration pattern to test the type of variable and assign the error to a new variable
                        if (contextFeature.Error is ValidationAppException exception)
                        {
                            await context.Response.WriteAsync(JsonSerializer.Serialize(new { exception.Errors }));
                        }
                        else
                        {
                            logger.LogError($"Something went wrong: {contextFeature.Error}");

                            await context.Response.WriteAsync(new ErrorDetails()
                            {
                                StatusCode = context.Response.StatusCode,
                                Message = contextFeature.Error.Message,
                            }.ToString());
                        }
                    }
                });
            });
        }
    }
}
