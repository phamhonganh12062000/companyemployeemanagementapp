﻿using CompanyEmployees.Presentation.Controllers;
using Contracts;
using Elasticsearch.Net;
using LoggerService;
using LoggerService.Sinks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Nest;
using Repository;
using Service;
using Service.Contracts;
using Marvin.Cache.Headers;
using AspNetCoreRateLimit;
using Entities.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Entities.ConfigurationModels;
using Microsoft.OpenApi.Models;
using Shared.Dtos.Employee;
using Service.DataShaping;
using Shared.Dtos.Company;
using Contracts.EntityLinkContracts;
using CompanyEmployees.Utility;
using CompanyEmployees.Presentation.ActionFilters;
using Shared.Static;
using Application.Behaviors;
using MediatR;
using FluentValidation;
using Microsoft.Extensions.Options;

namespace CompanyEmployees.Extensions
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Configure CORS to give/restrict access rights to applications from different domains
        /// </summary>
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", policyBuilder =>
                    policyBuilder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithExposedHeaders(Headers.XPagination));

                // To be more restrictive
                //options.AddPolicy("CorsPolicy", policyBuilder =>
                //    policyBuilder.WithOrigins("https://example.com")
                //    .WithMethods("POST", "GET")
                //    .WithHeaders("accept", "content-type"));
            });
        }

        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
                // Default options here, no need to specify anything
            });
        }

        public static void ConfigureLoggerService(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerManager, LoggerManager>();
        }

        public static void ConfigureUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        public static void ConfigureServiceManager(this IServiceCollection services)
        {
            services.AddScoped<IServiceManager, ServiceManager>();
        }

        public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<RepositoryContext>(options
                => options.UseSqlServer(configuration.GetConnectionString("sqlConnection"),
                    b => b.MigrationsAssembly(nameof(CompanyEmployees))));
        }

        public static IMvcBuilder AddCustomCSVFormatter(this IMvcBuilder builder)
        {
            return builder.AddMvcOptions(config => config.OutputFormatters.Add(new CsvOutpuFormatter()));
        }

        public static void ConfigureElasticSearch(this IServiceCollection services)
        {
            var pool = new SingleNodeConnectionPool(new Uri("https://localhost:5001"));
            var settings = new ConnectionSettings(pool).DefaultIndex("employees");
            var client = new ElasticClient(settings);
            services.AddSingleton(client);

        }

        public static void AddCustomMediaTypes(this IServiceCollection services)
        {
            services.Configure<MvcOptions>(config =>
            {
                var systemTextJsonOutputFormatter = config.OutputFormatters
                    .OfType<SystemTextJsonOutputFormatter>()?.FirstOrDefault();

                if (systemTextJsonOutputFormatter != null)
                {
                    systemTextJsonOutputFormatter.SupportedMediaTypes
                        .Add("application/vnd.codemaze.hateoas+json");

                    // Header for Mediator pattern
                    systemTextJsonOutputFormatter.SupportedMediaTypes
                        .Add("application-v2/vnd.codemaze.hateoas+json");
                    systemTextJsonOutputFormatter.SupportedMediaTypes
                        .Add("application/vnd.codemaze.apiroot+json");
                }

                var xmlOutputFormatter = config.OutputFormatters
                    .OfType<XmlDataContractSerializerOutputFormatter>()?.FirstOrDefault();

                if (xmlOutputFormatter != null)
                {
                    xmlOutputFormatter.SupportedMediaTypes
                        .Add("application/vnd.codemaze.hateoas+xml");

                    // Header for Mediator pattern
                    xmlOutputFormatter.SupportedMediaTypes
                        .Add("application-v2/vnd.codemaze.hateoas+xml");
                    xmlOutputFormatter.SupportedMediaTypes
                        .Add("application/vnd.codemaze.apiroot+xml");
                }
            });
        }

        public static void ConfigureVersioning(this IServiceCollection services)
        {
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.ApiVersionReader = new QueryStringApiVersionReader("api-version");
                options.Conventions.Controller<CompanyController>()
                    .HasApiVersion(new ApiVersion(1, 0));
                options.Conventions.Controller<CompanyV2Controller>()
                    .HasDeprecatedApiVersion(new ApiVersion(2, 0));
                options.Conventions.Controller<EmployeeController>()
                    .HasApiVersion(new ApiVersion(1, 0));
            });
        }

        public static void ConfigureResponseCaching(this IServiceCollection services) 
        {
            services.AddResponseCaching();
        }

        public static void ConfigureHttpCacheHeaders(this IServiceCollection services)
        {
            services.AddHttpCacheHeaders((expirationOptions) =>
            {
                expirationOptions.MaxAge = 65;
                expirationOptions.CacheLocation = CacheLocation.Private;
            },
            (validationOptions) =>
            {
                validationOptions.MustRevalidate = true;
            });
        }

        public static void ConfigureRateLimitingOptions(this IServiceCollection services)
        {
            var rateLimitRules = new List<RateLimitRule>
            {
                new RateLimitRule
                {
                    Endpoint = "*",
                    Limit = 30,
                    Period = "5m",
                },
            };
            services.Configure<IpRateLimitOptions>(option =>
            {
                option.GeneralRules = rateLimitRules;
            });
            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
            services.AddSingleton<IProcessingStrategy, AsyncKeyLockProcessingStrategy>();
        }

        /// <summary>
        /// Configure Identity for the specific `User` type.
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureIdentity(this IServiceCollection services)
        {
            var builder = services.AddIdentity<User, IdentityRole>(o =>
            {
                o.Password.RequireDigit = true;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 10;
                o.User.RequireUniqueEmail = true;
            })
                .AddEntityFrameworkStores<RepositoryContext>()
                .AddDefaultTokenProviders();
        }

        public static void AddJwtConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<JwtConfiguration>(configuration.GetSection("JwtSettings"));
        }

        public static void ConfigureJWT(this IServiceCollection services, IConfiguration configuration)
        {
            var jwtConfiguration = new JwtConfiguration();
            // Bind configuration values from a specific section (JWT config) to an instance of POCO
            configuration.Bind(jwtConfiguration.Section, jwtConfiguration);
            var secretKey = configuration["JWTSecretKey"];
            //TODO: Cannot set sys var due to admin right
            //var secretKey = Environment.GetEnvironmentVariable("SECRET");

            // Register JWT authentication middleware
            services.AddAuthentication(option =>
            {
                option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,

                        ValidIssuer = jwtConfiguration.ValidIssuer,
                        ValidAudience = jwtConfiguration.ValidAudience,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey)),
                    };
                });
        }

        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new OpenApiInfo 
                {
                    Title = "My App API",
                    Version = "v1",
                    Description = "CompanyManagementApp API by Hong Anh",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "John Doe",
                        Email = "johnDDoe@gmail.com",
                        Url = new Uri("https://x.com/johndoe"),
                    },
                    License = new OpenApiLicense
                    {
                        Name = "API LICX",
                        Url = new Uri("https://example.com/license"),
                    },
                });
                s.SwaggerDoc("v2", new OpenApiInfo { Title = "My App API", Version = "v2" });

                // Enabling XML comments
                var xmlFile = $"{typeof(Presentation.AssemblyReference).Assembly.GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                s.IncludeXmlComments(xmlPath);

                s.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Place to add JWT with Bearer",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                });

                s.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer",
                            },
                            Name = "Bearer",
                        },
                        new List<string>()
                    },
                });
            });
        }

        public static void ConfigureDataShaper(this IServiceCollection services)
        {
            services.AddScoped<IDataShaper<EmployeeDto>, DataShaper<EmployeeDto>>();
            services.AddScoped<IDataShaper<CompanyDto>, DataShaper<CompanyDto>>();
        }

        public static void ConfigureHATEOAS(this IServiceCollection services)
        {
            // Entity links
            services.AddScoped<IEmployeeLinks, EmployeeLinks>();
            services.AddScoped<ICompanyLinks, CompanyLinks>();

            // Validation for HATEOAS
            services.AddScoped<ValidateMediaTypeAttribute>();
        }

        public static void ConfigureActionFilters(this IServiceCollection services)
        {
            services.AddScoped<ValidationFilterAttribute>();
        }

        /// <summary>
        /// Scan the project assembly that contains the handlers and use them to handle business logic.
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureMediatR(this IServiceCollection services)
        {
            services.AddMediatR(typeof(Application.AssemblyReference).Assembly);
        }

        public static void ConfigureMediatorValidators(this IServiceCollection services)
        {
            // Register all validators inside the service collection
            services.AddValidatorsFromAssembly(typeof(Application.AssemblyReference).Assembly);

            // Register pipeline behavior
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
        }

        public static NewtonsoftJsonPatchInputFormatter GetJsonPatchInputFormatter()
        {
            return new ServiceCollection().AddLogging().AddMvc().AddNewtonsoftJson()
                .Services.BuildServiceProvider()
                .GetRequiredService<IOptions<MvcOptions>>().Value.InputFormatters
                .OfType<NewtonsoftJsonPatchInputFormatter>().First();
        }
    }
}
