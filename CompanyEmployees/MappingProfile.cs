﻿using AutoMapper;
using Entities.Models;
using Shared.Dtos.Company;
using Shared.Dtos.Employee;
using Shared.Dtos.User;

namespace CompanyEmployees
{
    public class MappingProfile : Profile
    {
        public MappingProfile() 
        {
            // GET: /api/companies
            CreateMap<Company, CompanyDto>().ForMember(c => c.FullAddress, 
                option => option.MapFrom(x => string.Join(' ', x.Address, x.Country)));

            // GET: /api/companies/{companyId}/employees
            CreateMap<Employee, EmployeeDto>();

            // POST: /api/companies
            CreateMap<CompanyForCreationDto, Company>();

            // POST: /api/companies/{companyId}/employees
            CreateMap<EmployeeForCreationDto, Employee>();

            // PUT: /api/companies/{companyId}/employees/{employeeId}
            // NOTE: Reverse mapping will be executed IF ASKED
            CreateMap<EmployeeForUpdateDto, Employee>().ReverseMap();

            // PUT: /api/companies/{companyId}
            CreateMap<CompanyForUpdateDto, Company>().ReverseMap();

            CreateMap<UserForRegistrationDto, User>();
        }
    }
}
