﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace CompanyEmployees.Migrations
{
    /// <inheritdoc />
    public partial class AddRolesToDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "8ec7fbd4-b2b7-45bf-a0ab-f202db8b18b0", "935a1070-e90c-41dc-90fc-b0a44b9dfc34", "Administrator", "ADMINISTRATOR" },
                    { "9bbac7ee-cbaa-4fef-9c0f-9012f2f23b3e", "6f3b595f-6440-4cab-ae97-6189f1b10a71", "Manager", "MANAGER" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "8ec7fbd4-b2b7-45bf-a0ab-f202db8b18b0");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9bbac7ee-cbaa-4fef-9c0f-9012f2f23b3e");
        }
    }
}
