﻿using CompanyEmployees.Presentation.Controllers;
using Contracts;
using Contracts.EntityLinkContracts;
using Entities.LinksModel;
using Entities.Models;
using Microsoft.Net.Http.Headers;
using Shared.Dtos.Company;
using Shared.Static;

namespace CompanyEmployees.Utility
{
    public class CompanyLinks : ICompanyLinks
    {
        private readonly LinkGenerator _linkGenerator;

        private readonly IDataShaper<CompanyDto> _dataShaper;

        public CompanyLinks(LinkGenerator linkGenerator, IDataShaper<CompanyDto> dataShaper)
        {
            _linkGenerator = linkGenerator;
            _dataShaper = dataShaper;
        }

        public LinkResponse TryGenerateLinks(IEnumerable<CompanyDto> companiesDto, string fields, HttpContext httpContext)
        {
            var shapedCompanies = ShapeDataForMultipleCompanies(companiesDto, fields);

            if (ShouldGenerateLinks(httpContext))
            {
                return ReturnLinkedCompanies(companiesDto, fields, httpContext, shapedCompanies);
            }

            return ReturnShapedCompanies(shapedCompanies);
        }

        private LinkResponse ReturnShapedCompanies(List<DynamicEntity> shapedCompanies)
        {
            return new LinkResponse { ShapedDynamicEntities = shapedCompanies };
        }

        private LinkResponse ReturnLinkedCompanies
            (IEnumerable<CompanyDto> companyDtos, string fields, HttpContext httpContext, List<DynamicEntity> shapedCompanies)
        {
            var companyDtosList = companyDtos.ToList();

            for (var index = 0; index < companyDtosList.Count; index++)
            {
                var companyLinks = CreateLinksForSingleCompany(httpContext, companyDtosList[index].Id, fields);

                shapedCompanies[index].Add("Links", companyLinks);
            }

            var companyWithLinksCollection = CreateLinksForMultipleCompanies(httpContext,
                new LinkCollectionWrapper<DynamicEntity>(shapedCompanies));

            return new LinkResponse { HasLinks = true, LinkedDynamicEntities = companyWithLinksCollection };
        }

        private List<Link> CreateLinksForSingleCompany(HttpContext httpContext, Guid id, string fields)
        {
            var acceptHeaderValue = GetAcceptHeaderValue(httpContext);

            List<Link>? links = null;

            links = acceptHeaderValue switch
            {
                "application-v2/vnd.codemaze.hateoas+json" or "application-v2/vnd.codemaze.hateoas+xml" => new List<Link>
                    {
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(CompanyV2Controller.GetSingleCompany), values: new
                            {
                                id, fields,
                            }) !,
                            "self",
                            Requests.GET),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(CompanyV2Controller.CreateSingleCompany), values: new
                            {
                                id,
                            }) !,
                            "add_company",
                            Requests.POST),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(CompanyV2Controller.DeleteSingleCompany), values: new
                            {
                                id,
                            }) !,
                            "delete_company",
                            Requests.DELETE),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(CompanyV2Controller.UpdateSingleCompany), values: new
                            {
                                id,
                            }) !,
                            "update_company",
                            Requests.PUT),
                    },
                _ => new List<Link>
                    {
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(CompanyController.GetCompany), values: new
                            {
                                id, fields,
                            }) !,
                            "self",
                            Requests.GET),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(CompanyController.CreateCompany), values: new
                            {
                                id,
                            }) !,
                            "add_company",
                            Requests.POST),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(CompanyController.DeleteCompany), values: new
                            {
                                id,
                            }) !,
                            "delete_company",
                            Requests.DELETE),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(CompanyController.UpdateCompany), values: new
                            {
                                id,
                            }) !,
                            "update_company",
                            Requests.PUT),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(CompanyController.PartiallyUpdateCompany), values: new
                            {
                                id,
                            }) !,
                            "partially_update_company",
                            Requests.PATCH),
                    },
            };
            return links;
        }

        private string GetAcceptHeaderValue(HttpContext httpContext)
        {
            return httpContext.Items.ElementAt(2).Value.ToString();
        }

        private LinkCollectionWrapper<DynamicEntity> CreateLinksForMultipleCompanies
            (HttpContext httpContext, LinkCollectionWrapper<DynamicEntity> companiesWrapper)
        {
            companiesWrapper.Links.Add(new Link(
                _linkGenerator.GetUriByAction(httpContext, nameof(CompanyController.GetAllCompanies), values: new
                {
                }) !,
                "self",
                "GET"));

            return companiesWrapper;
        }

        private bool ShouldGenerateLinks(HttpContext httpContext)
        {
            var mediaType = (MediaTypeHeaderValue)httpContext.Items["AcceptHeaderMediaType"];

            return mediaType.SubTypeWithoutSuffix.EndsWith("hateoas", StringComparison.InvariantCultureIgnoreCase);
        }

        private List<DynamicEntity> ShapeDataForMultipleCompanies(IEnumerable<CompanyDto> companyDtos, string fields)
        {
            return _dataShaper.ShapeDataForMultipleEntities(companyDtos, fields)
                .Select(c => c.DynamicEntity)
                .ToList();
        }
    }
}
