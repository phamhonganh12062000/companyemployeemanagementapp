﻿using CompanyEmployees.Presentation.Controllers;
using Contracts;
using Contracts.EntityLinkContracts;
using Entities.LinksModel;
using Entities.Models;
using Microsoft.Net.Http.Headers;
using Nest;
using Shared.Dtos.Employee;
using Shared.Static;
using System.ComponentModel.Design;

namespace CompanyEmployees.Utility
{
    public class EmployeeLinks : IEmployeeLinks
    {
        private readonly LinkGenerator _linkGenerator;
        private readonly IDataShaper<EmployeeDto> _dataShaper;

        public EmployeeLinks(LinkGenerator linkGenerator, IDataShaper<EmployeeDto> dataShaper)
        {
            _dataShaper = dataShaper;
            _linkGenerator = linkGenerator;
        }

        public LinkResponse TryGenerateLinks(IEnumerable<EmployeeDto> employeeDtos, string fields, 
            Guid companyId, HttpContext httpContext)
        {
            var shapedEmployees = ShapeDataForMultipleEmployees(employeeDtos, fields);

            if (ShouldGenerateLinks(httpContext))
            {
                return ReturnLinkedEmployees(employeeDtos, fields, companyId, httpContext, shapedEmployees);
            }

            return ReturnShapedEmployees(shapedEmployees);
        }

        private LinkResponse ReturnShapedEmployees(List<DynamicEntity> shapedEmployees)
        {
            return new LinkResponse { ShapedDynamicEntities = shapedEmployees };
        }

        private LinkResponse ReturnLinkedEmployees(IEnumerable<EmployeeDto> employeeDtos, string fields,
            Guid companyId, HttpContext httpContext, List<DynamicEntity> shapedEmployees)
        {
            var employeeDtosList = employeeDtos.ToList();

            for (var index = 0; index < employeeDtosList.Count; index++)
            {
                var employeeLinks = CreateLinksForSingleEmployee(httpContext, companyId, employeeDtosList[index].Id, fields);

                shapedEmployees[index].Add("Links", employeeLinks);
            }

            var employeeWithLinksCollection = CreateLinksForMultipleEmployees(httpContext,
                new LinkCollectionWrapper<DynamicEntity>(shapedEmployees));

            return new LinkResponse { HasLinks = true, LinkedDynamicEntities = employeeWithLinksCollection };
        }

        private LinkCollectionWrapper<DynamicEntity> CreateLinksForMultipleEmployees(HttpContext httpContext,
            LinkCollectionWrapper<DynamicEntity> employeesWrapper)
        {
            employeesWrapper.Links.Add(new Link(
                _linkGenerator.GetUriByAction(httpContext, nameof(EmployeeController.GetAllEmployeesOfACompany), values: new 
                {
                })!,
                "self",
                Requests.GET));

            return employeesWrapper;
        }

        private List<Link> CreateLinksForSingleEmployee(HttpContext httpContext, Guid companyId, Guid id, string fields)
        {
            var acceptHeaderValue = GetAcceptHeaderValue(httpContext);

            List<Link>? links = null;

            links = acceptHeaderValue switch
            {
                "application-v2/vnd.codemaze.hateoas+json" or "application-v2/vnd.codemaze.hateoas+xml" => new List<Link>
                    {
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(EmployeeV2Controller.GetSingleEmployeeOfACompany), values: new
                            {
                                companyId, id, fields
                            }) !,
                            "self",
                            Requests.GET),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(EmployeeV2Controller.CreateSingleEmployeeForACompany), values: new
                            {
                                companyId, id
                            }) !,
                            "add_employee",
                            Requests.POST),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(EmployeeV2Controller.DeleteSingleEmployeeOfACompany), values: new
                            {
                                companyId,
                                id
                            }) !,
                            "delete_employee",
                            Requests.DELETE),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(EmployeeV2Controller.UpdateSingleEmployeeOfACompany), values: new
                            {
                                companyId,
                                id
                            }) !,
                            "update_employee",
                            Requests.PUT),
                    },
                _ => new List<Link>
                    {
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(EmployeeController.GetSpecificEmployeeOfACompany), values: new
                            {
                                companyId, id, fields
                            }) !,
                            "self",
                            Requests.GET),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(EmployeeController.CreateEmployeeForCompany), values: new
                            {
                                companyId, id
                            }) !,
                            "add_employee",
                            Requests.POST),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(EmployeeController.DeleteEmployeeForCompany), values: new
                            {
                                companyId,
                                id
                            }) !,
                            "delete_employee",
                            Requests.DELETE),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(EmployeeController.UpdateEmployeeForCompany), values: new
                            {
                                companyId,
                                id
                            }) !,
                            "update_employee",
                            Requests.PUT),
                        new Link(
                            _linkGenerator.GetUriByAction(httpContext, nameof(EmployeeController.PartiallyUpdateEmployeeForCompany),values: new
                            {
                                companyId, id
                            }) !,
                            "partially_update_employee",
                            Requests.PATCH),
                    },
            };
            return links;
        }

        private string GetAcceptHeaderValue(HttpContext httpContext)
        {
            return httpContext.Items.ElementAt(2).Value.ToString();
        }

        private bool ShouldGenerateLinks(HttpContext httpContext)
        {
            var mediaType = (MediaTypeHeaderValue) httpContext.Items["AcceptHeaderMediaType"];

            return mediaType.SubTypeWithoutSuffix.EndsWith("hateoas", StringComparison.InvariantCultureIgnoreCase);
        }

        private List<DynamicEntity> ShapeDataForMultipleEmployees(IEnumerable<EmployeeDto> employeeDtos, string fields)
        {
            return _dataShaper.ShapeDataForMultipleEntities(employeeDtos, fields)
                .Select(e => e.DynamicEntity)
                .ToList();
        }

    }
}
