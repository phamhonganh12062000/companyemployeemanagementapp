﻿using Entities.ErrorModels;
using Entities.Responses;
using Entities.Responses.BadRequest;
using Entities.Responses.NotFound;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shared.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyEmployees.Presentation.Controllers
{
    public class ApiControllerBase : ControllerBase
    {
        [NonAction]
        public IActionResult ProcessError(ApiBaseResponse baseResponse)
        {
            return baseResponse switch
            {
                ApiNotFoundResponse => NotFound(new ErrorDetails
                {
                    Message = ((ApiNotFoundResponse)baseResponse).Message,
                    StatusCode = StatusCodes.Status404NotFound,
                }),
                ApiBadRequestResponse => BadRequest(new ErrorDetails
                {
                    Message = ((ApiBadRequestResponse)baseResponse).Message,
                    StatusCode = StatusCodes.Status400BadRequest,
                }),

                // Discards = unassigned variables
                // Skip the value by not creating a variable explicitly.
                // Used when the returned values of a function is not needed to improve memory allocation
                _ => throw new NotImplementedException()
            };
        }
    }
}
