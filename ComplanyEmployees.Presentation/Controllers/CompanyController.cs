﻿using Application.Queries;
using CompanyEmployees.Presentation.ActionFilters;
using CompanyEmployees.Presentation.Extensions;
using CompanyEmployees.Presentation.ModelBinders;
using Entities.LinksModel;
using Entities.Responses;
using Entities.Responses.Success;
using Marvin.Cache.Headers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Shared.Dtos.Company;
using Shared.RequestFeatures;
using Shared.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CompanyEmployees.Presentation.Controllers
{
    [ApiVersion("1.0")]
    [Route(EndPoints.Company)]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]

    // [ResponseCache(CacheProfileName = "120SecondsDuration")]
    public class CompanyController : ApiControllerBase
    {
        private readonly IServiceManager _service;
        private readonly ISender _sender;

        public CompanyController(IServiceManager service, ISender sender)
        {
            _service = service;
            _sender = sender;
        }

        /// <summary>
        /// Get the list of all companies
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "GetAllCompanies")]
        [HttpHead]
        [Authorize(Roles = Roles.Manager)]
        [ServiceFilter(typeof(ValidateMediaTypeAttribute))]
        public async Task<IActionResult> GetAllCompanies([FromQuery] CompanyParameters companyParameters)
        {
            var linkParams = new LinkParameters
            {
                CompanyParameters = companyParameters,
                HttpContext = HttpContext,
            };

            var (result, metaData) = await _service.CompanyService.GetAllCompaniesDtosAsync(linkParams, trackChanges: false);

            Response.Headers.Add(Headers.XPagination, JsonSerializer.Serialize(metaData));

            if (!result.Success)
            {
                return ProcessError(result);
            }

            var companiesDtos = result.GetResult<LinkResponse>();

            return companiesDtos.HasLinks ? Ok(companiesDtos.LinkedDynamicEntities) : Ok(companiesDtos.ShapedDynamicEntities);
        }

        /// <summary>
        /// Get information about a specific company
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}", Name ="GetCompany")]
        [HttpCacheExpiration(CacheLocation = CacheLocation.Public, MaxAge = 60)]
        [HttpCacheValidation(MustRevalidate = false)]
        public async Task<IActionResult> GetCompany(Guid id)
        {
            var result = await _service.CompanyService.GetCompanyDtoAsync(id, trackChanges: false);

            if (!result.Success)
            {
                return ProcessError(result);
            }

            return Ok(result.GetResult<CompanyDto>());
        }

        /// <summary>
        /// Get information about specified companies
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpGet("collection/{ids}", Name = "GetMultipleCompanies")]
        [ServiceFilter(typeof(ValidateMediaTypeAttribute))]
        public async Task<IActionResult> GetMultipleCompanies([ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> ids, [FromQuery] CompanyParameters companyParameters)
        {
            var linkParams = new LinkParameters
            {
                CompanyParameters = companyParameters,
                HttpContext = HttpContext,
            };

            var result = await _service.CompanyService.GetMultipleCompaniesByIdsAsync(ids, linkParams, trackChanges: false);

            if (!result.Success)
            {
                return ProcessError(result);
            }

            var companyCollectionDtos = result.GetResult<LinkResponse>();

            return companyCollectionDtos.HasLinks ? Ok(companyCollectionDtos.LinkedDynamicEntities) : Ok(companyCollectionDtos.ShapedDynamicEntities);
        }

        /// <summary>
        /// Add a company
        /// </summary>
        /// <param name="companyToCreate"></param>
        /// <returns>A new company added to the list</returns>
        /// <response code="201">Return the newly added company</response>
        /// <response code="400">If the request body is null</response>
        /// <response code="422">If the model is invalid</response>
        [HttpPost(Name = "CreateCompany")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        public async Task<IActionResult> CreateCompany([FromBody] CompanyForCreationDto companyToCreate)
        {
            var result = await _service.CompanyService.CreateCompanyDtoAsync(companyToCreate);

            if (!result.Success)
            {
                return ProcessError(result);
            }

            var createdCompanyDto = result.GetResult<CompanyDto>();

            return CreatedAtRoute(nameof(GetCompany), new { id = createdCompanyDto.Id }, createdCompanyDto);
        }

        /// <summary>
        /// Add multiple companies
        /// </summary>
        /// <param name="companyCollection"></param>
        /// <returns></returns>
        [HttpPost("collection")]
        public async Task<IActionResult> CreateMultipleCompanies([FromBody] IEnumerable<CompanyForCreationDto> companyCollection)
        {
            var (companies, ids) = await _service.CompanyService.CreateCompanyCollectionDtosAsync(companyCollection);

            return CreatedAtRoute(nameof(GetMultipleCompanies), new { ids }, companies);
        }

        /// <summary>
        /// Remove a company from the db
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCompany(Guid id)
        {
            await _service.CompanyService.DeleteCompanyAsync(id, trackChanges: false);

            return NoContent();
        }

        /// <summary>
        /// Update information about a specific company.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="companyForUpdate"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> UpdateCompany(Guid id, [FromBody] CompanyForUpdateDto companyForUpdate)
        {

            await _service.CompanyService.UpdateCompanyAsync(id, companyForUpdate, trackChanges: true);

            return NoContent();
        }

        /// <summary>
        /// Partially update information about a specific company.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="patchDoc"></param>
        /// <returns></returns>
        [HttpPatch("{id:guid}")]
        public async Task<IActionResult> PartiallyUpdateCompany(Guid id, [FromBody] JsonPatchDocument<CompanyForUpdateDto> patchDoc)
        {
            if (patchDoc is null)
            {
                return BadRequest(nameof(patchDoc) + " object sent from client is null.");
            }

            var (companyToPatchDto, companyToPatchFromDb) = await _service.CompanyService.GetCompanyForPatchAsync(id, trackChanges: true);

            patchDoc.ApplyTo(companyToPatchDto);

            TryValidateModel(companyToPatchDto);

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }

            await _service.CompanyService.SaveChangesForPatchAsync(companyToPatchDto, companyToPatchFromDb);

            return NoContent();

        }

        /// <summary>
        /// Get headers.
        /// </summary>
        /// <returns></returns>
        [HttpOptions]
        public IActionResult GetCompaniesOptions()
        {
            Response.Headers.Add("Allow", "GET, POST, OPTIONS");

            return Ok();
        }

        // TODO: Rs how to use this for different requests
        private IActionResult HandleResult(ApiBaseResponse result, IActionResult successAction)
        {
            if (!result.Success)
            {
                return ProcessError(result);
            }

            return successAction;
        }
    }
}
