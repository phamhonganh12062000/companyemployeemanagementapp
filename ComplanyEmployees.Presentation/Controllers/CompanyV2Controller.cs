﻿using Application.Commands;
using Application.Commands.Company;
using Application.Notifications;
using Application.Queries;
using Application.Queries.Company;
using CompanyEmployees.Presentation.ActionFilters;
using CompanyEmployees.Presentation.Extensions;
using CompanyEmployees.Presentation.ModelBinders;
using Entities.LinksModel;
using Entities.Responses;
using Marvin.Cache.Headers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Shared.Dtos.Company;
using Shared.RequestFeatures;
using Shared.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CompanyEmployees.Presentation.Controllers
{
    [ApiVersion("2.0", Deprecated = true)]
    [Route("api/{v:apiversion}/companies")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v2")]
    public class CompanyV2Controller : ApiControllerBase
    {
        private readonly ISender _sender;
        private readonly IPublisher _publisher; // Send messages (notifications) to specific topics

        public CompanyV2Controller(ISender sender, IPublisher publisher)
        {
            _sender = sender;
            _publisher = publisher;
        }

        /// <summary>
        /// Get the list of all companies with MediatR.
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "GetAllCompaniesV2")]
        [Authorize(Roles = Roles.Manager)]
        [ServiceFilter(typeof(ValidateMediaTypeAttribute))]
        public async Task<IActionResult> GetAllCompanies([FromQuery] CompanyParameters companyParameters)
        {
            var linkParams = ReturnLinkParams(companyParameters);

            var (result, metaData) = await _sender.Send(new GetAllCompaniesQuery(linkParams, TrackChanges: false));

            Response.Headers.Add(Headers.XPagination, JsonSerializer.Serialize(metaData));

            return CheckAndGetResponse(result);
        }

        /// <summary>
        /// Get the list of multiple companies with specified Ids with MediatR.
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpGet("collection/{ids}", Name = "GetMultipleCompaniesV2")]
        [ServiceFilter(typeof(ValidateMediaTypeAttribute))]
        public async Task<IActionResult> GetMultipleCompanies(
            [ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> ids,
            [FromQuery] CompanyParameters companyParameters)
        {
            var linkParams = ReturnLinkParams(companyParameters);

            var result = await _sender.Send(new GetMultipleCompaniesQuery(ids, linkParams, TrackChanges: false));

            return CheckAndGetResponse(result);
        }

        /// <summary>
        /// Get information about a specific company with MediatR.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}", Name = "GetSingleCompanyV2")]
        [HttpCacheExpiration(CacheLocation = CacheLocation.Public, MaxAge = 60)]
        [HttpCacheValidation(MustRevalidate = false)]
        public async Task<IActionResult> GetSingleCompany(Guid id)
        {
            var result = await _sender.Send(new GetSingleCompanyQuery(id, TrackChanges: false));

            if (!result.Success)
            {
                return ProcessError(result);
            }

            return Ok(result.GetResult<CompanyDto>());
        }

        /// <summary>
        /// Add a new company with MediatR.
        /// </summary>
        /// <param name="companyForCreationDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateSingleCompany([FromBody] CompanyForCreationDto companyForCreationDto)
        {
            if (companyForCreationDto is null)
            {
                return BadRequest(nameof(CompanyForCreationDto) + " object is null");
            }

            var companyToBeCreated = await _sender.Send(new CreateSingleCompanyCommand(companyForCreationDto));

            return CreatedAtRoute("GetSingleCompanyV2", new { id = companyToBeCreated.Id }, companyToBeCreated);
        }

        /// <summary>
        /// Add multiple companies with MediatR.
        /// </summary>
        /// <param name="companyForCreationDtos"></param>
        /// <returns></returns>
        [HttpPost("collection")]
        public async Task<IActionResult> CreateMultipleCompanies([FromBody] IEnumerable<CompanyForCreationDto> companyForCreationDtos)
        {
            if (companyForCreationDtos is null)
            {
                return BadRequest(nameof(CompanyForCreationDto) + " object is null");
            }

            var (companiesToBeCreated, ids) = await _sender.Send(new CreateMultipleCompaniesCommand(companyForCreationDtos));

            return CreatedAtRoute("GetMultipleCompaniesV2", new { ids }, companiesToBeCreated);
        }


        /// <summary>
        /// Update information about a single company with MediatR.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="companyForUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateSingleCompany(Guid id, CompanyForUpdateDto companyForUpdateDto)
        {
            if (companyForUpdateDto is null)
            {
                return BadRequest(nameof(CompanyForUpdateDto) + " object is null");
            }

            await _sender.Send(new UpdateSingleCompanyCommand(id, companyForUpdateDto, TrackChanges: true));

            return NoContent();
        }


        /// <summary>
        /// Remove a company from the database with MediatR
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteSingleCompany(Guid id)
        {
            await _publisher.Publish(new CompanyDeletedNotification(id, TrackChanges: false));

            return NoContent();
        }

        private LinkParameters ReturnLinkParams(CompanyParameters? companyParameters)
        {
            return new LinkParameters
            {
                CompanyParameters = companyParameters,
                HttpContext = HttpContext,
            };
        }

        private IActionResult CheckAndGetResponse(ApiBaseResponse response)
        {
            if (!response.Success)
            {
                return ProcessError(response);
            }

            var responseDataWithLinks = response.GetResult<LinkResponse>();

            return responseDataWithLinks.HasLinks ? Ok(responseDataWithLinks.LinkedDynamicEntities) : Ok(responseDataWithLinks.ShapedDynamicEntities);
        }
    }
}
