﻿using CompanyEmployees.Presentation.ActionFilters;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.RequestFeatures;
using System.Text.Json;
using Entities.LinksModel;
using Shared.Static;
using CompanyEmployees.Presentation.Extensions;
using Shared.Dtos.Company;
using CompanyEmployees.Presentation.ModelBinders;

namespace CompanyEmployees.Presentation.Controllers
{
    [ApiVersion("1.0")]
    [Route(EndPoints.Employee)]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]
    public class EmployeeController : ApiControllerBase
    {

        private readonly IServiceManager _service;

        public EmployeeController(IServiceManager service)
        {
            _service = service;
        }

        [HttpGet]
        [HttpHead]
        [ServiceFilter(typeof(ValidateMediaTypeAttribute))]
        public async Task<IActionResult> GetAllEmployeesOfACompany(Guid companyId, [FromQuery] EmployeeParameters employeeParameters)
        {
            var linkParams= ReturnLinkParams(employeeParameters);

            var (result, metaData) = await _service.EmployeeService.GetAllEmployeesOfACompanyDtosAsync(
                companyId, linkParams, trackChanges: false);

            Response.Headers.Add(Headers.XPagination, JsonSerializer.Serialize(metaData));

            if (!result.Success)
            {
                return ProcessError(result);
            }

            var employeesWithLinksDtos = result.GetResult<LinkResponse>();

            return employeesWithLinksDtos.HasLinks ? Ok(employeesWithLinksDtos.LinkedDynamicEntities) : Ok(employeesWithLinksDtos.ShapedDynamicEntities);
        }

        [HttpGet("collection/{ids}", Name = "GetMultipleEmployeesForCompany")]
        [ServiceFilter(typeof(ValidateMediaTypeAttribute))]
        public async Task<IActionResult> GetMultipleEmployeesOfACompany(
            Guid companyId,
            [ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> ids,
            [FromQuery] EmployeeParameters employeeParameters)
        {
            var linkParams = ReturnLinkParams(employeeParameters);

            var result = await _service.EmployeeService.GetMultipleEmployeesOfACompanyDtoAsync(companyId, ids, linkParams, trackChanges: false);

            if (!result.Success)
            {
                return ProcessError(result);
            }

            var employeesWithLinksDtos = result.GetResult<LinkResponse>();

            return employeesWithLinksDtos.HasLinks ? Ok(employeesWithLinksDtos.LinkedDynamicEntities) : Ok(employeesWithLinksDtos.ShapedDynamicEntities);
        }

        [HttpGet("{id:guid}", Name = "GetEmployeeForCompany")]
        public async Task<IActionResult> GetSpecificEmployeeOfACompany(Guid companyId, Guid id)
        {
            var result = await _service.EmployeeService.GetEmployeeOfACompanyDtoAsync(companyId, id, trackChanges: false);

            if (!result.Success)
            {
                return ProcessError(result);
            }

            var employeeDto = result.GetResult<EmployeeDto>();

            return Ok(employeeDto);
        }

        [HttpPost]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        public async Task<IActionResult> CreateEmployeeForCompany(Guid companyId, [FromBody] EmployeeForCreationDto employee)
        {

            var result = await _service.EmployeeService.CreateEmployeeForACompanyDtoAsync(companyId, employee, trackChanges: false);

            if (!result.Success)
            {
                return ProcessError(result);
            }

            var createdEmployeeDto = result.GetResult<EmployeeDto>();

            // Do a GET request back to the GetSpecificEmployeeOfACompany() method to retrieve the newly created entity
            return CreatedAtRoute("GetEmployeeForCompany", new { companyId, id = createdEmployeeDto.Id }, createdEmployeeDto);
        }

        [HttpPost("collection")]
        public async Task<IActionResult> CreateMultipleEmployeesForCompany(Guid companyId, [FromBody] IEnumerable<EmployeeForCreationDto> employees)
        {
            var (employeesToBeCreated, ids) = await _service.EmployeeService.CreateEmployeeCollectionForACompanyDtoAsync(companyId, employees, trackChanges: false);

            return CreatedAtRoute("GetMultipleEmployeesForCompany", new { companyId, ids }, employeesToBeCreated);
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeForCompany(Guid companyId, Guid id)
        {
            await _service.EmployeeService.DeleteEmployeeForACompanyAsync(companyId, id, false);

            return NoContent();
        }

        [HttpPut("{id:guid}")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> UpdateEmployeeForCompany(Guid companyId, Guid id, [FromBody] EmployeeForUpdateDto employeeForUpdateDto)
        {

            await _service.EmployeeService.UpdateEmployeeForACompanyDtoAsync(companyId, id, employeeForUpdateDto, companyTrackChanges: false, employeeTrackChanges: true);

            return NoContent();
        }

        [HttpPatch("{id:guid}")]
        public async Task<IActionResult> PartiallyUpdateEmployeeForCompany(Guid companyId, Guid id, [FromBody] JsonPatchDocument<EmployeeForUpdateDto> patchDoc)
        {
            // NOTE: The Content-Type header must be application/json-patch+json here
            if (patchDoc is null)
            {
                return BadRequest(nameof(patchDoc) + " object sent from client is null.");
            }

            var (employeeToPatchDto, employeeFromDb) = await _service.EmployeeService.GetEmployeeForPatchDtoAsync(companyId, id, companyTrackChanges: false, employeeTrackChanges: true);

            patchDoc.ApplyTo(employeeToPatchDto, ModelState);

            // Prevent an invalid employee from being saved to the db
            TryValidateModel(employeeToPatchDto);

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }

            await _service.EmployeeService.SaveChangesForPatchAsync(employeeToPatchDto, employeeFromDb);

            return NoContent();
        }

        [HttpOptions]
        public IActionResult GetEmployeesOptions()
        {
            Response.Headers.Add("Allow", "GET, POST, OPTIONS");

            return Ok();
        }

        private LinkParameters ReturnLinkParams(EmployeeParameters employeeParameters)
        {
            return new LinkParameters
            {
                EmployeeParameters = employeeParameters,
                HttpContext = HttpContext,
            };
        }
    }
}
