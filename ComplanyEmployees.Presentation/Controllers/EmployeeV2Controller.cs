﻿using Application.Commands.Company;
using Application.Commands.Employee;
using Application.Notifications;
using Application.Queries.Employee;
using CompanyEmployees.Presentation.ActionFilters;
using CompanyEmployees.Presentation.Extensions;
using CompanyEmployees.Presentation.ModelBinders;
using Entities.LinksModel;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Shared.Dtos.Company;
using Shared.Dtos.Employee;
using Shared.RequestFeatures;
using Shared.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CompanyEmployees.Presentation.Controllers
{
    [ApiVersion("2.0", Deprecated = true)]
    [Route("api/{v:apiversion}/companies/{companyId}/employees")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v2")]
    public class EmployeeV2Controller : ApiControllerBase
    {
        private readonly ISender _sender;
        private readonly IPublisher _publisher;

        public EmployeeV2Controller(ISender sender, IPublisher publisher)
        {
            _sender = sender;
            _publisher = publisher;
        }

        [HttpGet]
        [ServiceFilter(typeof(ValidateMediaTypeAttribute))]
        public async Task<IActionResult> GetAllEmployeesOfACompany(Guid companyId, [FromQuery] EmployeeParameters employeeParameters)
        {
            var linkParams = ReturnLinkParams(employeeParameters);

            var (result, metaData) = await _sender.Send(new GetAllEmployeesQuery(companyId, linkParams, TrackChanges: false));

            Response.Headers.Add(Headers.XPagination, JsonSerializer.Serialize(metaData));

            if (!result.Success)
            {
                return ProcessError(result);
            }

            var employeesWithLinksDtos = result.GetResult<LinkResponse>();

            return employeesWithLinksDtos.HasLinks ? Ok(employeesWithLinksDtos.LinkedDynamicEntities) : Ok(employeesWithLinksDtos.ShapedDynamicEntities);
        }

        [HttpGet("collection/{ids}", Name = "GetMultipleEmployeesForCompanyV2")]
        [ServiceFilter(typeof(ValidateMediaTypeAttribute))]
        public async Task<IActionResult> GetMultipleEmployeesOfACompany(
            Guid companyId,
            [ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> ids,
            [FromQuery] EmployeeParameters employeeParameters)
        {
            var linkParams = ReturnLinkParams(employeeParameters);

            var result = await _sender.Send(new GetMultipleEmployeesQuery(companyId, ids, linkParams, TrackChanges: false));

            if (!result.Success)
            {
                return ProcessError(result);
            }

            var employeesWithLinksDtos = result.GetResult<LinkResponse>();

            return employeesWithLinksDtos.HasLinks ? Ok(employeesWithLinksDtos.LinkedDynamicEntities) : Ok(employeesWithLinksDtos.ShapedDynamicEntities);

        }

        [HttpGet("{id:guid}", Name = "GetEmployeeForCompanyV2")]
        public async Task<IActionResult> GetSingleEmployeeOfACompany(Guid companyId, Guid id)
        {
            var result = await _sender.Send(new GetSingleEmployeeQuery(companyId, id, TrackChanges: false));

            if (!result.Success)
            {
                return ProcessError(result);
            }

            return Ok(result.GetResult<EmployeeDto>());
        }

        [HttpPost]
        public async Task<IActionResult> CreateSingleEmployeeForACompany(Guid companyId, [FromBody] EmployeeForCreationDto employee)
        {
            var result = await _sender.Send(new CreateSingleEmployeeCommand(companyId, employee, TrackChanges: false));

            if (!result.Success)
            {
                return ProcessError(result);
            }

            var createdEmployeeDto = result.GetResult<EmployeeDto>();

            return CreatedAtRoute("GetEmployeeForCompanyV2", new { companyId, id = createdEmployeeDto.Id }, createdEmployeeDto);
        }

        [HttpPost("collection")]
        public async Task<IActionResult> CreateMultipleEmployeesForACompany(Guid companyId, [FromBody] IEnumerable<EmployeeForCreationDto> employees)
        {
            var (employeesToBeCreated, ids) = await _sender.Send(new CreateMultipleEmployeesCommand(companyId, employees, TrackChanges: false));

            return CreatedAtRoute("GetMultipleEmployeesForCompanyV2", new { companyId, ids }, employeesToBeCreated);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateSingleEmployeeOfACompany(Guid companyId, Guid id, [FromBody] EmployeeForUpdateDto employeeForUpdateDto)
        {
            if (employeeForUpdateDto is null)
            {
                return BadRequest(nameof(EmployeeForUpdateDto) + " object is null");
            }

            await _sender.Send(new UpdateSingleEmployeeCommand(companyId, id, employeeForUpdateDto, CompanyTrackChanges: false, EmployeeTrackChanges: true));

            return NoContent();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteSingleEmployeeOfACompany(Guid companyId, Guid id)
        {
            await _publisher.Publish(new EmployeeDeletedNotification(companyId, id, TrackChanges: false));

            return NoContent();
        }

        private LinkParameters ReturnLinkParams(EmployeeParameters employeeParameters)
        {
            return new LinkParameters
            {
                EmployeeParameters = employeeParameters,
                HttpContext = HttpContext,
            };
        }
    }
}
