﻿using Entities.LinksModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Shared.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyEmployees.Presentation.Controllers
{
    [Route("api")]
    [ApiController]
    public class RootController : ControllerBase
    {
        private readonly LinkGenerator _linkGenerator;

        public RootController(LinkGenerator linkGenerator)
        {
            _linkGenerator = linkGenerator;
        }

        [HttpGet(Name ="GetRoot")]
        public IActionResult GetRoot([FromHeader(Name = "Accept")] string mediaType)
        {
            if (mediaType.Contains("application/vnd.codemaze.apiroot"))
            {
                var list = new List<Link>
                {
                    new Link
                    {
                        Href = _linkGenerator.GetUriByName(HttpContext, nameof(GetRoot), new { }),
                        Rel = "self",
                        Method = Requests.GET,
                    },

                    // NOTE: We can only create docs for ROOT LEVEL actions only (api/companies)
                    new Link
                    {
                        Href = _linkGenerator.GetUriByName(HttpContext, nameof(CompanyController.GetAllCompanies), new { }),
                        Rel = "companies",
                        Method = Requests.GET,
                    },
                    new Link
                    {
                        Href = _linkGenerator.GetUriByName(HttpContext, nameof(CompanyController.CreateCompany), new { }),
                        Rel = "create_company",
                        Method = Requests.GET,
                    },
                };

                return Ok(list);
            }

            return NoContent();
        }
    }
}
