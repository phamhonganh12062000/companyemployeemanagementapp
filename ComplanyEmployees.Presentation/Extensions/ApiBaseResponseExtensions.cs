﻿using Entities.Responses;
using Entities.Responses.Success;

namespace CompanyEmployees.Presentation.Extensions
{
    public static class ApiBaseResponseExtensions
    {
        /// <summary>
        /// Casting response of different entities
        /// </summary>
        /// <typeparam name="TResultType"></typeparam>
        /// <param name="response"></param>
        /// <returns></returns>
        public static TResultType GetResult<TResultType>(this ApiBaseResponse response)
        {
            return ((ApiOkResponse<TResultType>)response).Result;
        }
    }
}
