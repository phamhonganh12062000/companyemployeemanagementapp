﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel;
using System.Reflection;

namespace CompanyEmployees.Presentation.ModelBinders
{
    /// <summary>
    /// Bind the `string` type parameter to the `IEnumerable<Guid> argument`.
    /// </summary>
    public class ArrayModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {

            // Check if param is of the type Enumarable
            if (!IsBindingContextEnumerableType(bindingContext))
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return Task.CompletedTask;
            }

            // Extract a comma-separated string of GUIDs
            var providedValue = ExtractStringOfGuids(bindingContext);

            // Check if null => null check in the action in the controller
            if (string.IsNullOrEmpty(providedValue))
            {
                bindingContext.Result = ModelBindingResult.Success(null);
                return Task.CompletedTask;
            }

            // Store the type the context consists of
            var contextType = GetBindingContextType(bindingContext.ModelType);

            bindingContext.Model = ConvertToGuidArray(providedValue, contextType);

            bindingContext.Result = ModelBindingResult.Success(bindingContext.Model);

            return Task.CompletedTask;
        }

        private bool IsBindingContextEnumerableType(ModelBindingContext bindingContext)
        {
            return bindingContext.ModelMetadata.IsEnumerableType;
        }

        private string ExtractStringOfGuids(ModelBindingContext bindingContext)
        {
            return bindingContext.ValueProvider.GetValue(bindingContext.ModelName).ToString();
        }

        private Type GetBindingContextType(Type contextType)
        {
            return contextType.GetTypeInfo().GenericTypeArguments[0];
        }

        private Array ConvertToGuidArray(string stringOfGuids, Type contextType)
        {
            // Create a converter for the GUID type
            var converter = TypeDescriptor.GetConverter(contextType);

            var tempArr = stringOfGuids.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => converter.ConvertFromString(x.Trim()))
                .ToArray();
            
            var guidArr = Array.CreateInstance(contextType, tempArr.Length);
            // Copy the value of ojectArr to the Guid arr and assign it to the bindingContext
            tempArr.CopyTo(guidArr, 0);

            return guidArr;
        }
    }
}
