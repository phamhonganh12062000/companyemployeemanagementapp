﻿using Entities.Models;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Contracts.EntityContracts
{
    public interface ICompanyRepository
    {
        Task<PagedList<Company>> GetAllCompaniesAsync(CompanyParameters companyParameters, bool trackChanges);

        Task<IEnumerable<Company>> GetMultipleCompaniesByIdsAsync(IEnumerable<Guid> ids, CompanyParameters companyParameters, bool trackChanges);

        Task<Company> GetCompanyAsync(Guid companyId, bool trackChanges);

        void CreateCompany(Company company);

        void DeleteCompany(Company company);
    }
}
