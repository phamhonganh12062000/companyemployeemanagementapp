﻿using Entities.Models;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.EntityContracts
{
    public interface IEmployeeRepository
    {
        Task<PagedList<Employee>> GetAllEmployeesOfACompanyAsync(Guid companyId, EmployeeParameters employeeParameters, bool trackChanges);

        Task<IEnumerable<Employee>> GetMultipleEmployeesOfACompanyByIdsAsync(Guid companyId, IEnumerable<Guid> ids, EmployeeParameters employeeParameters,  bool trackChanges);

        Task<Employee> GetEmployeeOfACompanyAsync(Guid companyId, Guid id, bool trackChanges);

        void CreateEmployeeForACompany(Guid companyId, Employee employee);

        void DeleteEmployee(Employee employee);
    }
}
