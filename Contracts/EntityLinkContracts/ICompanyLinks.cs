﻿using Entities.LinksModel;
using Microsoft.AspNetCore.Http;
using Shared.Dtos.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.EntityLinkContracts
{
    public interface ICompanyLinks
    {
        LinkResponse TryGenerateLinks(IEnumerable<CompanyDto> companiesDto, string fields, HttpContext httpContext);
    }
}
