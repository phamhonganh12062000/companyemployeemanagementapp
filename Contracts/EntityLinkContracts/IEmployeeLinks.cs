﻿using Entities.LinksModel;
using Microsoft.AspNetCore.Http;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.EntityLinkContracts
{
    public interface IEmployeeLinks
    {
        LinkResponse TryGenerateLinks(IEnumerable<EmployeeDto> employeeDtos, string fields, Guid companyId, HttpContext httpContext);
    }
}
