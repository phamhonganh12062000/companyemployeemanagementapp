﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts.EntityContracts;

namespace Contracts
{
    public interface IUnitOfWork
    {
        ICompanyRepository CompanyRepository { get; }

        IEmployeeRepository EmployeeRepository { get; }

        Task SaveAsync();
    }
}
