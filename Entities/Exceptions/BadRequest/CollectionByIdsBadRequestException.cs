﻿using System.Runtime.Serialization;

namespace Entities.Exceptions.BadRequest
{
    public sealed class CollectionByIdsBadRequestException : BadRequestException
    {
        public CollectionByIdsBadRequestException() : base("Parameters ids is null.")
        {
        }
    }
}