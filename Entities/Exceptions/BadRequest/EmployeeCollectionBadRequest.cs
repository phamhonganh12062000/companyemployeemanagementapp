﻿using Entities.Exceptions.BadRequest;
using System.Runtime.Serialization;

namespace Entities.Exceptions.BadRequest
{
    public sealed class EmployeeCollectionBadRequest : BadRequestException
    {
        public EmployeeCollectionBadRequest() : base("Employee collection sent from a client is null.")
        {
        }
    }
}