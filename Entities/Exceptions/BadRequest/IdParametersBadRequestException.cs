﻿using Entities.Exceptions.BadRequest;
using System.Runtime.Serialization;

namespace Entities.Exceptions.BadRequest
{
    public sealed class IdParametersBadRequestException : BadRequestException
    {
        public IdParametersBadRequestException() : base("Collection count mismatch comparing to ids.")
        {
        }
    }
}