﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.LinksModel
{
    /// <summary>
    /// Wrap links in this class for presentation purposes.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LinkCollectionWrapper<T> : LinkResourceBase
    {
        public List<T> Value { get; set; } = new List<T>();

        public LinkCollectionWrapper()
        {   
        }

        public LinkCollectionWrapper(List<T> value)
        {
            Value = value;
        }
    }
}
