﻿using Microsoft.AspNetCore.Http;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.LinksModel
{
    public record LinkParameters
    {
        public EmployeeParameters EmployeeParameters { get; init; }

        public CompanyParameters CompanyParameters { get; init; }

        public HttpContext HttpContext { get; init; }
    }

}
