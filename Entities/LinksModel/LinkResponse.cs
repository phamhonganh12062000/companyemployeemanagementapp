﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.LinksModel
{
    /// <summary>
    /// Check if the response has links
    /// </summary>
    public class LinkResponse
    {
        public bool HasLinks { get; set; }
        public List<DynamicEntity> ShapedDynamicEntities { get; set; }
        public LinkCollectionWrapper<DynamicEntity> LinkedDynamicEntities { get; set; }

        public LinkResponse()
        {
            LinkedDynamicEntities = new LinkCollectionWrapper<DynamicEntity>();
            ShapedDynamicEntities = new List<DynamicEntity>();
        }
    }
}
