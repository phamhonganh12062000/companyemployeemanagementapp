﻿using Entities.LinksModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Entities.Models
{
    public class DynamicEntity : DynamicObject, IXmlSerializable, IDictionary<string, object>
    {
        private readonly string _root = "DynamicEntity";
        private readonly IDictionary<string, object> _expando;

        public DynamicEntity()
        {
            _expando = new ExpandoObject();
        }

        /// <summary>
        /// Handle dynamic property access when using the `dynamic` keyword
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public override bool TryGetMember(GetMemberBinder binder, out object? result)
        {
            // Check if the property exists and access the value of the property
            if (_expando.TryGetValue(binder.Name, out object value))
            {
                result = value;
                return true;
            }

            // Let the base class handle property access
            return base.TryGetMember(binder, out result);
        }

        public override bool TrySetMember(SetMemberBinder binder, object? value)
        {
            _expando[binder.Name] = value;
            return true;
        }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Read the XML document and populate the Expando object dynamically
        /// The populating process is based on the element/property names and their associated data types
        /// </summary>
        /// <param name="reader"></param>
        public void ReadXml(XmlReader reader)
        {
            // Read the root element of the XML
            reader.ReadStartElement(_root);

            while (!reader.Name.Equals(_root))
            {
                string typeContent; // Hold the content of the `type` attribute of the XML element being processed
                Type underlyingType; // Store the .NET `Type` based on the content of the `type` attribute of the XML
                var name = reader.Name; // Hold the name of the current XML element being processed

                reader.MoveToAttribute("type");
                typeContent = reader.ReadContentAsString();
                underlyingType = Type.GetType(typeContent);
                reader.MoveToContent(); // Move the `XmlReader` back to the content of the current element

                // Read the element content as a value of a data specified by `underlyingType` variable
                // Then assign the read element content to the `_expando` object with the property name `name` as the key
                _expando[name] = reader.ReadElementContentAs(underlyingType, null);
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (var key in _expando.Keys)
            {
                var value = _expando[key];
                WriteLinksToXml(key, value, writer);
            }
        }

        private void WriteLinksToXml(string key, object value, XmlWriter writer)
        {
            writer.WriteStartElement(key);

            if (value.GetType() == typeof(List<Link>))
            {
                foreach (var val in value as List<Link>)
                {
                    writer.WriteStartElement(nameof(Link));
                    // NOTE: This method is called recursively for each of the properties of the Link class
                    WriteLinksToXml(nameof(val.Href), val.Href, writer);
                    WriteLinksToXml(nameof(val.Rel), val.Rel, writer);
                    WriteLinksToXml(nameof(val.Method), val.Method, writer);
                    writer.WriteEndElement();
                }
            }
            else
            {
                writer.WriteString(value.ToString());
            }

            writer.WriteEndElement();
        }

        public void Add(string key, object value)
        {
            _expando.Add(key, value);
        }

        public bool ContainsKey(string key)
        {
            return _expando.ContainsKey(key);
        }

        public ICollection<string> Keys
        {
            get { return _expando.Keys; }
        }

        public ICollection<object> Values
        {
            get { return _expando.Values; }
        }

        public bool Remove(string key)
        {
            return _expando.Remove(key);
        }

        public bool TryGetValue(string key, out object value)
        {
            return _expando.TryGetValue(key, out value);
        }

        /// <summary>
        /// Define an indexer for the class itself
        /// A convenient way to work with dynamic properties stored in the `_expando` object using string keys
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object this[string key]
        {
            get
            {
                return _expando[key];
            }

            set
            {
                _expando[key] = value;
            }
        }

        public void Add(KeyValuePair<string, object> item)
        {
            _expando.Add(item);
        }

        public void Clear()
        {
            _expando.Clear();
        }

        public bool Contains(KeyValuePair<string, object> item)
        {
            return _expando.Contains(item);
        }

        public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
        {
            _expando.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _expando.Count; }
        }

        public bool IsReadOnly
        {
            get { return _expando.IsReadOnly; }
        }

        public bool Remove(KeyValuePair<string, object> item)
        {
            return _expando.Remove(item);
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return _expando.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

}
