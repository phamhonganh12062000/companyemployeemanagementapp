﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    public class ShapedDynamicEntity
    {
        public Guid Id { get; set; }

        public DynamicEntity DynamicEntity { get; set; }

        public ShapedDynamicEntity()
        {
            DynamicEntity = new DynamicEntity();
        }
    }
}
