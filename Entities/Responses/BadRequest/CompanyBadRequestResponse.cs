﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Responses.BadRequest
{
    public sealed class CompanyBadRequestResponse : ApiBadRequestResponse
    {
        public CompanyBadRequestResponse(Guid id) : base($"Request to the company with id: {id} was invalid.")
        {
            
        }
    }
}
