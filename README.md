# CompanyEmployeeManagementApp


## Description
A personal WebAPI project to manage companies and employees.

## Project status
- [x] Configure a Logging Service (Serilog)
- [x] Implement Onion Architecture
- [x] Handle different request types
    - [x] GET
    - [x] POST
    - [x] PUT
    - [x] DELETE
    - [x] OPTIONS
    - [x] PATCH
    - [x] HEAD
- [x] Handle global error
- [x] Handle single resource
- [x] Add validations
- [x] Add action filters
- [x] Add paging
- [x] Add filtering
- [x] Add searching
- [x] Add sorting
- [x] Add data shaping
- [x] Add support for HATEOAS (Hypermedia as the Engine of Application State)
- [x] Add root document
- [x] Add API versioning
- [x] Add caching
- [x] Add rate limiting & throttling
- [x] Add JWT, Identity, Refresh Token
- [x] Add binding configuration & options pattern
- [x] Document API with Swagger
- [ ] Deploy to IIS
- [x] Improve performance with ApiBaseResponse
- [x] Add CQRS and MediatR

## TODOs after main course
- [x] Apply StyleCop analyzer with custom configs
- [x] Add features for Company/Entity entity
    - [x] Data Shaping
    - [x] Paging (Company)
    - [x] Filtering (Company)
    - [x] Sorting
    - [x] Searching (Company)
- [x] Add `CompanyLinks.cs` with HATEOAS
- [x] Apply PATCH for Company entity + Update `CompanyLinks.cs`
- [x] Apply `ApiBaseResponse` to all service interfaces + implementations to improve performance
- [x] Apply OPTIONS for Employee + HEAD for Company
- [x] Add static class for Headers + Requests
- [x] Add action `GetMultipleEmployeesOfACompany` + `CreateMultipleEmployeesForACompany`
- [x] Apply CQRS + Mediator pattern for both Company and Employee (All commands + queries + handlers)
    - [x] Company
        - [x] GetMultipleCompanies
        - [x] CreateMultipleCompanies
        - [x] DeleteSingleCompany
    - [x] Employee
        - [x] GetAllEmployeesOfACompany
        - [x] GetMultipleEmployeesOfACompany
        - [x] GetSingleEmployeeOfACompany
        - [x] CreateSingleEmployee
        - [x] CreateMultipleEmployees
        - [x] DeleteSingleEmployee
        - [x] UpdateSingleEmployee
- [x] Refactor services in `Program.cs` into `ServiceExtensions.cs`
- [x] Update Links for `CompanyV2Controller.cs` + `EmployeeV2Controller.cs`
    - [x] Company
        - [x] GetAllCompanies
    - [x] Employee
        - [x] GetAllEmployeesOfACompany
- [x] Refactor big functions to smaller ones for cleaner code
  - [x] `ValidationBehavior.cs`
  - [x] `EmployeeLinks.cs`
  - [x] `OrderQueryBuilder.cs`
  - [ ] 
  - [ ] ...
- [x] Update README

## TODOS after Security course
- [ ] Integrate Duende IdentityServer to the CompanyEmployees app



