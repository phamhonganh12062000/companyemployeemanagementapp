﻿using Contracts.EntityContracts;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Repository.Extensions;
using Shared.RequestFeatures;

namespace Repository.EntityRepository
{
    public class CompanyRepository : RepositoryBase<Company>, ICompanyRepository
    {
        public CompanyRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public async Task<PagedList<Company>> GetAllCompaniesAsync(CompanyParameters companyParameters, bool trackChanges)
        {
            var companiesFromDb = await GetAll(trackChanges)
                .Filter(companyParameters.Country)
                .Search(companyParameters.SearchTerm)
                .Sort(companyParameters.OrderBy)
                .Page(companyParameters.PageNumber, companyParameters.PageSize)
                .ToListAsync();

            var count = companiesFromDb.Count;

            return PagedList<Company>.ToPagedList(companiesFromDb, count, companyParameters.PageNumber, companyParameters.PageSize);
        }

        public async Task<Company> GetCompanyAsync(Guid companyId, bool trackChanges)
        {
            return await GetByCondition(c => c.Id.Equals(companyId), trackChanges).SingleOrDefaultAsync();
        }

        public void CreateCompany(Company company)
        {
            Create(company);
        }

        public async Task<IEnumerable<Company>> GetMultipleCompaniesByIdsAsync(IEnumerable<Guid> ids, CompanyParameters companyParameters, bool trackChanges)
        {
             return await GetByCondition(c => ids.Contains(c.Id), trackChanges)
                .Sort(companyParameters.OrderBy)
                .ToListAsync();
        }

        public void DeleteCompany(Company company)
        {
            Delete(company);
        }
    }
}
