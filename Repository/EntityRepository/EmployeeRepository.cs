﻿using Contracts;
using Contracts.EntityContracts;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Repository.Extensions;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.EntityRepository
{
    public class EmployeeRepository : RepositoryBase<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }

        public async Task<PagedList<Employee>> GetAllEmployeesOfACompanyAsync
            (Guid companyId, EmployeeParameters employeeParameters, bool trackChanges)
        {
            var employeesFromDb = await GetByCondition(e => e.CompanyId.Equals(companyId), trackChanges)
                .Filter(employeeParameters.MinAge, employeeParameters.MaxAge)
                .Search(employeeParameters.SearchTerm)
                .Sort(employeeParameters.OrderBy)
                // NOTE: The db handles the pagination and returns only the data required for a specific page
                .Page(employeeParameters.PageNumber, employeeParameters.PageSize)
                .ToListAsync();

            var count = await GetByCondition(e => e.CompanyId.Equals(companyId), trackChanges).CountAsync();

            return PagedList<Employee>.ToPagedList(employeesFromDb, count, employeeParameters.PageNumber, employeeParameters.PageSize);
        }

        public async Task<IEnumerable<Employee>> GetMultipleEmployeesOfACompanyByIdsAsync(Guid companyId, IEnumerable<Guid> ids, EmployeeParameters employeeParameters, bool trackChanges)
        {
            return await GetByCondition(e => e.CompanyId.Equals(companyId) && ids.Contains(e.Id), trackChanges)
                .Sort(employeeParameters.OrderBy)
                .ToListAsync();
        }

        public async Task<Employee> GetEmployeeOfACompanyAsync(Guid companyId, Guid id, bool trackChanges)
        {
            return await GetByCondition(e =>
                e.CompanyId.Equals(companyId) && e.Id.Equals(id), trackChanges).SingleOrDefaultAsync();
        }

        public void CreateEmployeeForACompany(Guid companyId, Employee employee)
        {
            employee.CompanyId = companyId;
            Create(employee);
        }

        public void DeleteEmployee(Employee employee)
        {
            Delete(employee);
        }


    }
}
