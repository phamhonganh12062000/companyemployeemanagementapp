﻿using Entities.Models;
using Repository.Extensions.Utility;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Extensions
{
    public static class RepositoryCompanyExtensions
    {
        public static IQueryable<Company> Filter(this IQueryable<Company> companies, string queryCountry)
        {
            if (string.IsNullOrWhiteSpace(queryCountry))
            {
                return companies;
            }

            return companies.Where(c => c.Country == queryCountry);
        }

        public static IQueryable<Company> Search(this IQueryable<Company> companies, string searchTerm)
        {
            if (string.IsNullOrWhiteSpace(searchTerm))
            {
                return companies;
            }

            var lowerCaseTerm = searchTerm.Trim().ToLower();

            return companies.Where(c => c.Name.ToLower().Contains(lowerCaseTerm)
                || c.Address.ToLower().Contains(lowerCaseTerm));
        }

        public static IQueryable<Company> Sort(this IQueryable<Company> companies, string orderByQueryString)
        {
            if (string.IsNullOrWhiteSpace(orderByQueryString))
            {
                return companies.OrderBy(c => c.Name);
            }

            var orderQuery = OrderQueryBuilder.CreateOrderQuery<Company>(orderByQueryString);

            if (string.IsNullOrWhiteSpace(orderQuery))
            {
                return companies.OrderBy(c => c.Name);
            }

            return companies.OrderBy(orderQuery);
        }

        public static IQueryable<Company> Page(this IQueryable<Company> companies, int pageNumber, int pageSize)
        {
            return companies.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }
    }
}
