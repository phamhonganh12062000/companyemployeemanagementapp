﻿using Entities.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Extensions.Utility
{
    public static class OrderQueryBuilder
    {
        public static string CreateOrderQuery<T>(string orderByQueryString)
        {
            string[] orderParams = SplitOrderParamsFromQuery(orderByQueryString);

            PropertyInfo[] propertyInfos = GetPropertyInfos<T>();

            string orderQuery = BuildOrderQuery(orderParams, propertyInfos);

            return orderQuery;
        }

        private static string[] SplitOrderParamsFromQuery(string orderByQueryString)
        {
            return orderByQueryString.Trim().Split(',');
        }

        private static PropertyInfo[] GetPropertyInfos<TObject>()
        {
            return typeof(TObject).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        }

        private static PropertyInfo GetPropertyByName(PropertyInfo[] propertyInfos, string propertyNameFromQuery)
        {
            return propertyInfos.FirstOrDefault(pi => pi.Name.Equals(propertyNameFromQuery, StringComparison.InvariantCultureIgnoreCase));
        }

        private static string BuildOrderQuery(string[] orderParams, PropertyInfo[] propertyInfos)
        {
            StringBuilder orderQueryBuilder = new();

            foreach (var param in orderParams)
            {
                if (string.IsNullOrWhiteSpace(param))
                {
                    continue;
                }

                string propertyFromQueryName = param.Split(" ")[0];
                PropertyInfo objectProperty = GetPropertyByName(propertyInfos, propertyFromQueryName);

                if (objectProperty == null)
                {
                    // Continue to the next param if the previous one does not exist
                    continue;
                }

                var orderDirection = param.EndsWith(" desc") ? "descending" : "ascending";

                orderQueryBuilder.Append($"{objectProperty.Name.ToString()} {orderDirection}, ");

            }

            return orderQueryBuilder.ToString().TrimEnd(',', ' ');
        }
    }
}
