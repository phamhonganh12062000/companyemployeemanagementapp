﻿using Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected RepositoryContext RepositoryContext;

        public RepositoryBase(RepositoryContext repositoryContext)
        {
            RepositoryContext = repositoryContext;
        }

        public void Create(T entity)
        {
            RepositoryContext.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            RepositoryContext.Set<T>().Remove(entity);
        }

        public void Update(T entity)
        {
            RepositoryContext.Set<T>().Update(entity);
        }

        public IQueryable<T> GetAll(bool trackChanges)
        {
            if (!trackChanges)
            {
                return RepositoryContext.Set<T>().AsNoTracking();
            }
            else
            {
                return RepositoryContext.Set<T>();
            }
        }

        public IQueryable<T> GetByCondition(Expression<Func<T, bool>> condition, bool trackChanges)
        {
            if (!trackChanges)
            {
                return RepositoryContext.Set<T>()
                    .Where(condition)
                    .AsNoTracking();
            }
            else
            {
                return RepositoryContext.Set<T>()
                    .Where(condition);
            }
        }


    }
}
