﻿using Contracts;
using Contracts.EntityContracts;
using Repository.EntityRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly RepositoryContext _dbContext;

        // Use Lazy initialization so repository instances are available when we access them for the 1st time
        private readonly Lazy<ICompanyRepository> _companyRepository;
        private readonly Lazy<IEmployeeRepository> _employeeRepository;

        public UnitOfWork(RepositoryContext dbContext)
        {
            _dbContext = dbContext;
            _companyRepository = new Lazy<ICompanyRepository>(() => new CompanyRepository(_dbContext));
            _employeeRepository = new Lazy<IEmployeeRepository>(() => new EmployeeRepository(_dbContext));
        }

        // Getter Logic
        public ICompanyRepository CompanyRepository
        {
            get
            {
                return _companyRepository.Value;
            }
        }

        public IEmployeeRepository EmployeeRepository
        {
            get
            {
                return _employeeRepository.Value;
            }
        }

        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
