﻿using Entities.LinksModel;
using Entities.Models;
using Entities.Responses;
using Shared.Dtos.Company;
using Shared.Dtos.Employee;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Service.Contracts
{
    public interface ICompanyService
    {
        Task<(ApiBaseResponse response, MetaData metaData)> GetAllCompaniesDtosAsync(LinkParameters linkParameters, bool trackChanges);

        Task<ApiBaseResponse> GetMultipleCompaniesByIdsAsync(IEnumerable<Guid> ids, LinkParameters linkParameters, bool trackChanges);

        Task<ApiBaseResponse> GetCompanyDtoAsync(Guid companyId, bool trackChanges);

        Task<(CompanyForUpdateDto companyToPatch, Company companyEntity)> GetCompanyForPatchAsync(Guid companyId, bool trackChanges);

        Task<ApiBaseResponse> CreateCompanyDtoAsync(CompanyForCreationDto company);

        Task<(IEnumerable<CompanyDto> companies, string ids)> CreateCompanyCollectionDtosAsync(IEnumerable<CompanyForCreationDto> companyCollection);

        Task DeleteCompanyAsync(Guid companyId, bool trackChanges);

        Task UpdateCompanyAsync(Guid companyId, CompanyForUpdateDto companyForUpdate, bool trackChanges);

        Task SaveChangesForPatchAsync(CompanyForUpdateDto companyToPatch, Company company);
    }
}
