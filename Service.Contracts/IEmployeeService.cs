﻿using Entities.LinksModel;
using Entities.Models;
using Entities.Responses;
using Shared.Dtos.Employee;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Contracts
{
    public interface IEmployeeService
    {
        Task<(ApiBaseResponse response, MetaData metaData)> GetAllEmployeesOfACompanyDtosAsync
            (Guid companyId, LinkParameters linkParameters, bool trackChanges);

        Task<ApiBaseResponse> GetMultipleEmployeesOfACompanyDtoAsync(Guid companyId, IEnumerable<Guid> ids, LinkParameters linkParameters, bool trackChanges);

        Task<ApiBaseResponse> GetEmployeeOfACompanyDtoAsync(Guid companyId, Guid id, bool trackChanges);

        Task<ApiBaseResponse> CreateEmployeeForACompanyDtoAsync(Guid companyId, EmployeeForCreationDto employeeForCreation, bool trackChanges);

        Task<(IEnumerable<EmployeeDto> employees, string ids)> CreateEmployeeCollectionForACompanyDtoAsync
            (Guid companyId, IEnumerable<EmployeeForCreationDto> employeeCollection, bool trackChanges);

        Task DeleteEmployeeForACompanyAsync(Guid companyId, Guid id, bool trackChanges);

        Task UpdateEmployeeForACompanyDtoAsync(Guid companyId, Guid id, EmployeeForUpdateDto employeeForUpdateDto,
            bool companyTrackChanges, bool employeeTrackChanges);

        Task<(EmployeeForUpdateDto employeeToPatchDto, Employee employeeEntity)> GetEmployeeForPatchDtoAsync(
            Guid companyId, Guid id, bool companyTrackChanges, bool employeeTrackChanges);

        Task SaveChangesForPatchAsync(EmployeeForUpdateDto employeeToPatch, Employee employee);
    }
}
