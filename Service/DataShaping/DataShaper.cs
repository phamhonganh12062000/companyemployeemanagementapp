﻿using Contracts;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Service.DataShaping
{
    public class DataShaper<T> : IDataShaper<T> 
        where T : class // Generic type constraint
    {
        public PropertyInfo[] Properties { get; set; }

        public DataShaper()
        {
            Properties = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);
        }

        public IEnumerable<ShapedDynamicEntity> ShapeDataForMultipleEntities(IEnumerable<T> entities, string fieldsString)
        {
            return FetchDataForMultipleEntities(entities, GetRequiredProperties(fieldsString));
        }

        public ShapedDynamicEntity ShapeDataForSingleEntity(T entity, string fieldsString)
        {
            return FetchDataForSingleEntity(entity, GetRequiredProperties(fieldsString));
        }

        private IEnumerable<PropertyInfo> GetRequiredProperties(string fieldsString)
        {
            List<PropertyInfo> requiredProperties = new();

            if (!string.IsNullOrWhiteSpace(fieldsString))
            {
                var fields = fieldsString.Split(',', StringSplitOptions.RemoveEmptyEntries);

                foreach (var field in fields)
                {
                    var property = Properties.FirstOrDefault(pi =>
                        pi.Name.Equals(field.Trim(), StringComparison.InvariantCultureIgnoreCase));

                    if (property == null)
                    {
                        continue; // Continue with the next field
                    }

                    requiredProperties.Add(property);
                }
            }
            else
            {
                requiredProperties = Properties.ToList();
            }

            return requiredProperties;
        }

        private IEnumerable<ShapedDynamicEntity> FetchDataForMultipleEntities(IEnumerable<T> entities, 
            IEnumerable<PropertyInfo> requiredProperties)
        {
            var shapedData = new List<ShapedDynamicEntity>();

            foreach( var entity in entities)
            {
                var shapedObject = FetchDataForSingleEntity(entity, requiredProperties);
                shapedData.Add(shapedObject);
            }

            return shapedData;
        }

        private ShapedDynamicEntity FetchDataForSingleEntity(T entity, IEnumerable<PropertyInfo> requiredProperties)
        {
            // ExpandoObject implements IDictionary<string,object>
            var shapedObject = new ShapedDynamicEntity();

            foreach (var property in requiredProperties)
            {
                var objectPropertyValue = property.GetValue(entity);

                // Add property name as a key and its value as a value
                shapedObject.DynamicEntity.TryAdd(property.Name, objectPropertyValue);
            }

            var entityIdProperty = entity.GetType().GetProperty("Id");
            shapedObject.Id = (Guid)entityIdProperty.GetValue(entity);

            return shapedObject;
        }
    }
}
