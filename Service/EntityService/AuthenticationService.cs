﻿using AutoMapper;
using Contracts;
using Entities.ConfigurationModels;
using Entities.Exceptions.BadRequest;
using Entities.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Service.Contracts;
using Shared.Dtos.Authentication;
using Shared.Dtos.User;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Service.EntityService
{
    internal sealed class AuthenticationService : IAuthenticationService
    {
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration; // NOTE: This is a workaround for not able to set an environment variable
        private readonly IOptionsMonitor<JwtConfiguration> _jwtConfigOptions;
        private readonly JwtConfiguration _jwtConfiguration;

        private User? _user;

        public AuthenticationService(ILoggerManager logger, IMapper mapper,
            UserManager<User> userManager,
            IConfiguration configuration,
            IOptionsMonitor<JwtConfiguration> jwtConfigOptions)
        {
            _configuration = configuration;
            _jwtConfigOptions = jwtConfigOptions;
            _logger = logger;
            _mapper = mapper;
            _userManager = userManager;
            _jwtConfiguration = _jwtConfigOptions.CurrentValue;

        }

        public async Task<TokenDto> CreateToken(bool populateExp)
        {
            var signingCredentials = GetSigningCredentials();
            var claims = await GetClaims();
            var accessTokenOptions = GenerateAccessTokenOptions(signingCredentials, claims);

            var refreshToken = GenerateRefreshToken();

            _user.RefreshToken = refreshToken;

            if (populateExp)
            {
                _user.RefreshTokenExpiryDate = DateTime.Now.AddDays(7);
            }

            await _userManager.UpdateAsync(_user);

            var accessToken = new JwtSecurityTokenHandler().WriteToken(accessTokenOptions);

            return new TokenDto { AccessToken = accessToken, RefreshToken = refreshToken };
        }

        public async Task<IdentityResult> RegisterUserDtoAsync(UserForRegistrationDto userForRegistrationDto)
        {
            var user = _mapper.Map<User>(userForRegistrationDto);

            var result = await _userManager.CreateAsync(user, userForRegistrationDto.Password);

            if (result.Succeeded)
            {
                await _userManager.AddToRolesAsync(user, userForRegistrationDto.Roles);
            }

            return result;
        }

        public async Task<bool> ValidateUser(UserForAuthenticationDto userForAuthenticationDto)
        {
            _user = await _userManager.FindByNameAsync(userForAuthenticationDto.UserName);

            // NOTE: Here we verify the user's password against the HASHED password from the db
            var isUserValidated = (_user != null && await _userManager.CheckPasswordAsync(_user, userForAuthenticationDto.Password));

            if (!isUserValidated)
            {
                _logger.LogWarning($"{nameof(ValidateUser)}: Authentication failed. Wrong user name or password.");
            }

            return isUserValidated;
        }

        public async Task<TokenDto> RefreshToken(TokenDto tokenDto)
        {
            var principalClaims = GetClaimsPrincipalFromExpiredToken(tokenDto.AccessToken);

            var user = await _userManager.FindByNameAsync(principalClaims.Identity.Name);

            if (user == null
                || user.RefreshToken != tokenDto.RefreshToken
                || user.RefreshTokenExpiryDate <= DateTime.Now)
            {
                throw new RefreshTokenBadRequest();
            }

            _user = user;

            // NOTE: no updating the expiry date here
            return await CreateToken(populateExp: false);
        }

        private byte[] ReturnEncodedSecretKey()
        {
            return Encoding.UTF8.GetBytes(_configuration["JWTSecretKey"]);
        }

        private SymmetricSecurityKey ReturnSecurityKey()
        {
            var encodedSecretKey = ReturnEncodedSecretKey();

            return new SymmetricSecurityKey(encodedSecretKey);
        }

        /// <summary>
        /// Return secret key as a byte array.
        /// </summary>
        /// <returns></returns>
        private SigningCredentials GetSigningCredentials()
        {
            var securityKey = ReturnSecurityKey();

            return new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
        }

        private async Task<List<Claim>> GetClaims()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, _user.UserName)
            };

            var roles = await _userManager.GetRolesAsync(_user);

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            return claims;
        }

        private JwtSecurityToken GenerateAccessTokenOptions(SigningCredentials signingCredentials, List<Claim> claims)
        {

            return new JwtSecurityToken
            (
                issuer: _jwtConfiguration.ValidIssuer,
                audience: _jwtConfiguration.ValidAudience,
                claims: claims,
                expires: DateTime.Now.AddMinutes(Convert.ToDouble(_jwtConfiguration.Expires)),
                signingCredentials: signingCredentials
            );
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        private ClaimsPrincipal GetClaimsPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParams = new TokenValidationParameters
            {
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = ReturnSecurityKey(),
                ValidateLifetime = true,
                ValidIssuer = _jwtConfiguration.ValidIssuer,
                ValidAudience = _jwtConfiguration.ValidAudience,
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var principalClaims = tokenHandler.ValidateToken(token, tokenValidationParams, out SecurityToken securityToken);

            var jwtSecurityToken = (JwtSecurityToken)securityToken;

            if (jwtSecurityToken == null ||
                !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new SecurityTokenException("Invalid token.");
            }

            return principalClaims;
        }


    }
}
