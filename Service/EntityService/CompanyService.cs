﻿using AutoMapper;
using Contracts;
using Contracts.EntityLinkContracts;
using Entities.Exceptions;
using Entities.Exceptions.BadRequest;
using Entities.Exceptions.NotFound;
using Entities.LinksModel;
using Entities.Models;
using Entities.Responses;
using Entities.Responses.Success;
using Service.Contracts;
using Shared.Dtos.Company;
using Shared.RequestFeatures;
using System.ComponentModel.Design;
using System.Runtime.InteropServices;

namespace Service.EntityService
{
    internal sealed class CompanyService : ICompanyService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;
        private readonly IDataShaper<CompanyDto> _dataShaper;
        private readonly ICompanyLinks _companyLinks;

        public CompanyService(IUnitOfWork unitOfWork, ILoggerManager logger, IMapper mapper, IDataShaper<CompanyDto> dataShaper, ICompanyLinks companyLinks)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _dataShaper = dataShaper;
            _companyLinks = companyLinks;
        }

        /// <summary>
        /// Testing with ApiBaseResponse instead of IEnumerable<CompanyDto>
        /// </summary>
        /// <param name="trackChanges"></param>
        /// <returns></returns>
        public async Task<(ApiBaseResponse response, MetaData metaData)> GetAllCompaniesDtosAsync(LinkParameters linkParameters, bool trackChanges)
        {
            var companiesWithMetaData = await _unitOfWork.CompanyRepository.GetAllCompaniesAsync(linkParameters.CompanyParameters, trackChanges);

            var companiesDto = _mapper.Map<IEnumerable<CompanyDto>>(companiesWithMetaData);

            var companyWithLinks = _companyLinks.TryGenerateLinks(companiesDto, linkParameters.CompanyParameters.Fields, linkParameters.HttpContext);

            return (response: new ApiOkResponse<LinkResponse>(companyWithLinks), metaData: companiesWithMetaData.MetaData);
        }

        public async Task<ApiBaseResponse> GetMultipleCompaniesByIdsAsync(IEnumerable<Guid> ids, LinkParameters linkParameters, bool trackChanges)
        {
            if (ids is null)
            {
                throw new IdParametersBadRequestException();
            }

            var collectionOfCompanies = await _unitOfWork.CompanyRepository.GetMultipleCompaniesByIdsAsync(ids, linkParameters.CompanyParameters, trackChanges);

            if (ids.Count() != collectionOfCompanies.Count())
            {
                throw new CollectionByIdsBadRequestException();
            }

            var companyDtos = _mapper.Map<IEnumerable<CompanyDto>>(collectionOfCompanies);

            var companyWithLinks = _companyLinks.TryGenerateLinks(companyDtos, linkParameters.CompanyParameters.Fields, linkParameters.HttpContext);

            return new ApiOkResponse<LinkResponse>(companyWithLinks);
        }

        /// <summary>
        /// Testing with ApiBaseResponse instead of CompanyDto
        /// </summary>
        /// <param name="trackChanges"></param>
        /// <returns></returns>
        public async Task<ApiBaseResponse> GetCompanyDtoAsync(Guid id, bool trackChanges)
        {
            var companyFromDb = await GetCompanyAndCheckIfItExistsAsync(id, trackChanges);

            var companyDto = _mapper.Map<CompanyDto>(companyFromDb);

            return new ApiOkResponse<CompanyDto>(companyDto);
        }

        public async Task<(CompanyForUpdateDto companyToPatch, Company companyEntity)> GetCompanyForPatchAsync(Guid companyId, bool trackChanges)
        {
            var companyFromDb = await GetCompanyAndCheckIfItExistsAsync(companyId, trackChanges);

            var companyToPatch = _mapper.Map<CompanyForUpdateDto>(companyFromDb);

            return (companyToPatch, companyFromDb);
        }

        public async Task<ApiBaseResponse> CreateCompanyDtoAsync(CompanyForCreationDto company)
        {
            var companyToCreate = _mapper.Map<Company>(company);

            _unitOfWork.CompanyRepository.CreateCompany(companyToCreate);
            await _unitOfWork.SaveAsync();

            var companyDto = _mapper.Map<CompanyDto>(companyToCreate);

            return new ApiOkResponse<CompanyDto>(companyDto);
        }

        /// <summary>
        /// Create companies in bulk.
        /// </summary>
        /// <param name="companyCollection"></param>
        /// <returns>A Tuple with two fields (companies and ids)</returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<(IEnumerable<CompanyDto> companies, string ids)> CreateCompanyCollectionDtosAsync
            (IEnumerable<CompanyForCreationDto> companyCollection)
        {
            if (companyCollection is null)
            {
                throw new CompanyCollectionBadRequest();
            }

            var companiesToCreate = _mapper.Map<IEnumerable<Company>>(companyCollection);

            foreach (var company in companiesToCreate)
            {
                _unitOfWork.CompanyRepository.CreateCompany(company);
            }

            await _unitOfWork.SaveAsync();

            var companyCollectionToReturn = _mapper.Map<IEnumerable<CompanyDto>>(companiesToCreate);

            var ids = string.Join(",", companyCollectionToReturn.Select(c => c.Id));

            return (companies: companyCollectionToReturn, ids: ids);

        }

        public async Task DeleteCompanyAsync(Guid companyId, bool trackChanges)
        {

            _unitOfWork.CompanyRepository.DeleteCompany(await GetCompanyAndCheckIfItExistsAsync(companyId, trackChanges));
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateCompanyAsync(Guid companyId, CompanyForUpdateDto companyForUpdate, bool trackChanges)
        {
            var companyFromDb = await GetCompanyAndCheckIfItExistsAsync(companyId, trackChanges);

            _mapper.Map(companyForUpdate, companyFromDb);

            await _unitOfWork.SaveAsync();
        }

        private async Task<Company> GetCompanyAndCheckIfItExistsAsync(Guid id, bool trackChanges)
        {
            var company = await _unitOfWork.CompanyRepository.GetCompanyAsync(id, trackChanges);

            if (company is null)
            {
                throw new CompanyNotFoundException(id);
            }

            return company;
        }

        public async Task SaveChangesForPatchAsync(CompanyForUpdateDto companyToPatch, Company company)
        {
            _mapper.Map(companyToPatch, company);
            await _unitOfWork.SaveAsync();
        }
    }
}