﻿using AutoMapper;
using Contracts;
using Contracts.EntityLinkContracts;
using Entities.Exceptions;
using Entities.Exceptions.BadRequest;
using Entities.Exceptions.NotFound;
using Entities.LinksModel;
using Entities.Models;
using Entities.Responses;
using Entities.Responses.Success;
using Service.Contracts;
using Shared.Dtos.Company;
using Shared.Dtos.Employee;
using Shared.RequestFeatures;
using System.Dynamic;

namespace Service.EntityService
{
    internal sealed class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;
        private readonly IDataShaper<EmployeeDto> _dataShaper;
        private readonly IEmployeeLinks _employeeLinks;

        public EmployeeService(IUnitOfWork unitOfWork, ILoggerManager logger,
            IMapper mapper, IDataShaper<EmployeeDto> dataShaper, IEmployeeLinks employeeLinks)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _dataShaper = dataShaper;
            _employeeLinks = employeeLinks;
        }

        public async Task<(ApiBaseResponse response, MetaData metaData)> GetAllEmployeesOfACompanyDtosAsync
            (Guid companyId, LinkParameters linkParameters, bool trackChanges)
        {
            if (!linkParameters.EmployeeParameters.ValidAgeRange)
            {
                throw new MaxAgeRangeBadRequestException();
            }

            await CheckIfCompanyExists(companyId, trackChanges);

            var employeesWithMetaData = await _unitOfWork.EmployeeRepository
                .GetAllEmployeesOfACompanyAsync(companyId, linkParameters.EmployeeParameters, trackChanges);

            var employeeDtos = _mapper.Map<IEnumerable<EmployeeDto>>(employeesWithMetaData);

            var employeesWithLinks = _employeeLinks.TryGenerateLinks(employeeDtos,
                linkParameters.EmployeeParameters.Fields,
                companyId, linkParameters.HttpContext);

            return (response: new ApiOkResponse<LinkResponse>(employeesWithLinks), metaData: employeesWithMetaData.MetaData);
        }

        public async Task<ApiBaseResponse> GetMultipleEmployeesOfACompanyDtoAsync
            (Guid companyId, IEnumerable<Guid> ids, LinkParameters linkParameters, bool trackChanges)
        {
            if (ids is null)
            {
                throw new IdParametersBadRequestException();
            }

            await CheckIfCompanyExists(companyId, trackChanges);

            var collectionOfEmployees = await _unitOfWork.EmployeeRepository
                .GetMultipleEmployeesOfACompanyByIdsAsync(companyId, ids, linkParameters.EmployeeParameters, trackChanges);

            if (ids.Count() != collectionOfEmployees.Count())
            {
                throw new CollectionByIdsBadRequestException();
            }

            var employeeDtos = _mapper.Map<IEnumerable<EmployeeDto>>(collectionOfEmployees);

            var employeesWithLinks = _employeeLinks.TryGenerateLinks(employeeDtos,
                linkParameters.EmployeeParameters.Fields,
                companyId, linkParameters.HttpContext);

            return new ApiOkResponse<LinkResponse>(employeesWithLinks);

        }

        public async Task<ApiBaseResponse> GetEmployeeOfACompanyDtoAsync(Guid companyId, Guid id, bool trackChanges)
        {
            await CheckIfCompanyExists(companyId, trackChanges);

            var employeeFromDb = await GetEmployeeForCompanyAndCheckIfItExists(companyId, id, trackChanges);

            var employeeDto = _mapper.Map<EmployeeDto>(employeeFromDb);

            return new ApiOkResponse<EmployeeDto>(employeeDto);
        }

        public async Task<ApiBaseResponse> CreateEmployeeForACompanyDtoAsync(Guid companyId, EmployeeForCreationDto employeeForCreation, bool trackChanges)
        {
            await CheckIfCompanyExists(companyId, trackChanges);

            var employeeToBeCreated = _mapper.Map<Employee>(employeeForCreation);

            _unitOfWork.EmployeeRepository.CreateEmployeeForACompany(companyId, employeeToBeCreated);

            await _unitOfWork.SaveAsync();

            var employeeToReturn = _mapper.Map<EmployeeDto>(employeeToBeCreated);

            return new ApiOkResponse<EmployeeDto>(employeeToReturn);
        }

        public async Task<(IEnumerable<EmployeeDto> employees, string ids)> CreateEmployeeCollectionForACompanyDtoAsync
            (Guid companyId, IEnumerable<EmployeeForCreationDto> employeeCollection, bool trackChanges)
        {
            if (employeeCollection is null)
            {
                throw new EmployeeCollectionBadRequest();
            }

            await CheckIfCompanyExists(companyId, trackChanges);

            var employeesToCreate = _mapper.Map<IEnumerable<Employee>>(employeeCollection);

            foreach (var employee in employeesToCreate)
            {
                _unitOfWork.EmployeeRepository.CreateEmployeeForACompany(companyId, employee);
            }

            await _unitOfWork.SaveAsync();

            var employeeCollectionToReturn = _mapper.Map<IEnumerable<EmployeeDto>>(employeesToCreate);

            var ids = string.Join(",", employeeCollectionToReturn.Select(e => e.Id));

            return (employees: employeeCollectionToReturn, ids);
        }

        public async Task DeleteEmployeeForACompanyAsync(Guid companyId, Guid id, bool trackChanges)
        {
            await CheckIfCompanyExists(companyId, trackChanges);

            var employeeFromDb = await GetEmployeeForCompanyAndCheckIfItExists(companyId, id, trackChanges);

            _unitOfWork.EmployeeRepository.DeleteEmployee(employeeFromDb);
            await _unitOfWork.SaveAsync();
        }

        public async Task<(EmployeeForUpdateDto employeeToPatchDto, Employee employeeEntity)> GetEmployeeForPatchDtoAsync(Guid companyId,
            Guid id, bool companyTrackChanges, bool employeeTrackChanges)
        {
            await CheckIfCompanyExists(companyId, companyTrackChanges);

            var employeeFromDb = await GetEmployeeForCompanyAndCheckIfItExists(companyId, id, employeeTrackChanges);

            var employeeToPatchDto = _mapper.Map<EmployeeForUpdateDto>(employeeFromDb);

            return (employeeToPatchDto, employeeFromDb);
        }

        public async Task SaveChangesForPatchAsync(EmployeeForUpdateDto employeeToPatch, Employee employee)
        {
            _mapper.Map(employeeToPatch, employee);
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateEmployeeForACompanyDtoAsync
            (Guid companyId, Guid id, EmployeeForUpdateDto employeeForUpdateDto, bool companyTrackChanges, bool employeeTrackChanges)
        {
            await CheckIfCompanyExists(companyId, companyTrackChanges);

            // employeeTrackChanges will be true here so EF Core can track the changes on the employee entity
            var employee = await GetEmployeeForCompanyAndCheckIfItExists(companyId, id, employeeTrackChanges);

            // Connected update: Use the same context object to fetch the entity and to update it
            _mapper.Map(employeeForUpdateDto, employee);
            await _unitOfWork.SaveAsync();
        }

        private async Task CheckIfCompanyExists(Guid companyId, bool trackChanges)
        {
            var company = await _unitOfWork.CompanyRepository.GetCompanyAsync(companyId, trackChanges);

            if (company is null)
            {
                throw new CompanyNotFoundException(companyId);
            }
        }

        private async Task<Employee> GetEmployeeForCompanyAndCheckIfItExists(Guid companyId, Guid id, bool trackChanges)
        {
            // employeeTrackChanges will be true here so EF Core can track the changes on the employee entity
            var employee = await _unitOfWork.EmployeeRepository.GetEmployeeOfACompanyAsync(companyId, id, trackChanges);

            if (employee is null)
            {
                throw new EmployeeNotFoundException(id);
            }

            return employee;
        }
    }
}