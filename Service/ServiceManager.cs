﻿using AutoMapper;
using Contracts;
using Contracts.EntityLinkContracts;
using Entities.ConfigurationModels;
using Entities.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Service.Contracts;
using Service.EntityService;
using Shared.Dtos.Company;
using Shared.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public sealed class ServiceManager : IServiceManager
    {
        private readonly Lazy<ICompanyService> _companyService;
        private readonly Lazy<IEmployeeService> _employeeService;
        private readonly Lazy<IAuthenticationService> _authenticationService;

        public ServiceManager(IUnitOfWork unitOfWork,
            ILoggerManager logger,
            IMapper mapper,
            IDataShaper<EmployeeDto> employeeDataShaper,
            IDataShaper<CompanyDto> companyDataShaper,
            IEmployeeLinks employeeLinks,
            ICompanyLinks companyLinks,
            UserManager<User> userManager,
            IConfiguration configuration,
            IOptionsMonitor<JwtConfiguration> jwtConfigOptions)
        {
            _companyService = new Lazy<ICompanyService>(() => new CompanyService(unitOfWork, logger, mapper, companyDataShaper, companyLinks));
            _employeeService = new Lazy<IEmployeeService>(() => new EmployeeService(unitOfWork, logger, mapper, employeeDataShaper, employeeLinks));
            _authenticationService = new Lazy<IAuthenticationService>(() => new AuthenticationService(logger, mapper, userManager, configuration, jwtConfigOptions));
        }

        public ICompanyService CompanyService
        {
            get
            {
                return _companyService.Value;
            }
        }

        public IEmployeeService EmployeeService
        {
            get
            {
                return _employeeService.Value;
            }

        }

        public IAuthenticationService AuthenticationService
        {
            get
            {
                return _authenticationService.Value;
            }
        }
    }
}
