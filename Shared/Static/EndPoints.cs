﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Static
{
    public static class EndPoints
    {
        public const string Token = "api/token";
        public const string Authentication = "api/authentication";
        public const string Company = "api/companies";
        public const string Employee = "api/companies/{companyId}/employees";
    }
}
