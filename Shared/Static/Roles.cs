﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Static
{
    public static class Roles
    {
        public const string Administrator = "Administrator";
        public const string Manager = "Manager";
    }
}
