using Contracts.EntityContracts;
using Entities.Models;
using Moq;
using Shared.RequestFeatures;

namespace Tests;

public class CompanyRepositoryTests
{
    [Fact]
    public void GetAllCompaniesAsync_ReturnListsOfCompanies_WithASingleCompany()
    {
        // Arrange
        var mockRepo = new Mock<ICompanyRepository>();
        mockRepo.Setup(repo => (repo.GetAllCompaniesAsync(null,false)))
            .Returns(Task.FromResult(GetAllCompanies()));

        // Act
        var result = mockRepo.Object.GetAllCompaniesAsync(null, false)
            .GetAwaiter()
            .GetResult()
            .ToList();

        // Assert
        Assert.IsType<List<Company>>(result);
        Assert.Single(result);
    }

    private PagedList<Company> GetAllCompanies()
    {
        return new PagedList<Company>(
    new List<Company>
        {
            new Company
            {
                Id = Guid.NewGuid(),
                Name = "Test Company",
                Country = "United States",
                Address = "908 Woodrow Way",
            },
        },1, 1, 1);

    }
}